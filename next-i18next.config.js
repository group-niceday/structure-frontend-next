module.exports = {
  i18n: {
    defaultLocale: 'ko',
    locales: ['ko', 'en'],
    defaultNS: 'common',
    localeDetection: false,
  },
};

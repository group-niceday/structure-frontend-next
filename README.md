# 프론트엔드 개발 기준(structure-frontend-naxt)
* frontend 개발을 위한 기본 개발환경을 제공한다.


# 이력
* v0.0.1
    * 2022.10.11
        * 최초 등록

# 구성
* 환경
    * windows 10, mac
    * IntelliJ IDEA 2022.2.3 (Ultimate Edition)
    * node v16.14.0
    * npm v8.19.1

<!-- blank line -->
* 라이브러리

# 설치 및 실행
<!-- blank line -->
* 설치
    * Node 모듈 설치 (<a href="https://nodejs.org/ko/">Link</a>)
    * Git 설치 (<a href="https://git-scm.com/">Link</a>)
    * Webstrom 설치 (<a href="https://www.jetbrains.com/ko-kr/webstorm/download/#section=windows">Link</a>)

* 실행
    * npm i
    * npm run dev

## WebStorm
* 코드 spaces 셋팅
    * Preferences > Editor > Code Style
    * HTML > Tabs and Indents
        * Tab Size : 2
        * Indent : 2
        * Continuation Indent: 4
    * Typescript > Tabs and Indents
        * Tab Size : 2
        * Indent : 2
        * Continuation Indent: 2
    * Typescript > Punctuation
        * use "Single" quotes "in new Code"
* Key Mapping 셋팅
    * "Reformat Code" 항목 단축키 등록

# 개발
<!-- blank line -->
## 패키지 및 파일 명명 규칙
* 모든 화면 파일명(*.tsx)은 "kebab-case"로 작성한다.
    * Good: sample-list.tsx
    * Bad: SampleList.tsx
* 모든 화면 파일명(*.tsx)은 기능의 해당하는 두가지 단어 이상으로 작성한다.
    * Good: sample-list.tsx
    * Bad: sample.tsx
* 모든 기능 파일명(*.ts)은 "kebab-case" + "dot.case"로 작성한다.
    * Good: platform.service.ts
    * Bad: platform-service.ts
    * 기능 ex) service, model, router, service...
* 모든 폴더는 "kebab-case"로 작성한다.
    * Good: platform, tv-platform
    * Bad: TvPlatform, tvPlatform
* 기타 파일은 "kebab-case"로 작성한다.
    * Good: style.css, default-style.css
    * Bad: defaultStyle.css
  
## 패키지 구조
  ```
  ├── public
  ├── src
  │   ├── app
  │   │   ├── {{비즈니스}}
  │   │   │    ├── components
  │   │   │    ├── model
  │   │   │    ├── store
  │   │   │    │   ├── reducer
  │   │   │    │   └── slice
  │   │   │    └── view          
  │   │   ├── system
  │   │   │   ├── layout
  │   │   │   └── view
  │   │   └── shared
  │   │       ├── axios
  │   │       ├── enum
  │   │       └── util
  │   ├── assets
  │   │   ├── css
  │   │   ├── font
  │   │   └── images
  │   │       ├── common
  │   │       └── icon
  │   ├── core
  │   │   ├── decorator
  │   │   │   ├── description.decorator.ts
  │   │   │   └── validate.decorator.ts
  │   │   └── service
  │   │       ├── description.decorator.ts
  │   │       └── validate.decorator.ts
  │   ├── pages
  │   │   ├── {{비즈니스}}
  │   │   │   ├── detail
  │   │   │   │   └── [id].tsx 
  │   │   │   ├── modify
  │   │   │   │   └── [id].tsx 
  │   │   │   ├── add.tsx    
  │   │   │   └── index.tsx 
  │   │   ├── _app.tsx
  │   │   └── index.tsx   
  │   ├── hooks.ts
  │   ├── reducer.ts
  │   └── store.ts
  ├── static
  ├── .gitignore
  ├── env.development
  ├── env.product
  ├── env.local
  ├── package.json
  ├── readme.md
  ├── tsconfig.json
  └── tslint.json
  ```
## 상위 패키지 구조 설명


## 패키지 구조 설명


## 화면 별 패키지 구조


## 화면 별 패키지 구조 설명


# 코딩 명명 규칙
## 함수명


## Action


## 접근제어자
* 모든 public 접근제어자는 생략한다.
* 화면에서 사용하는 변수, 함수는 public 이다.
* class 내부적으로 사용하는 함수, 변수는 private를 명시적으로 선언한다.


# 예제
## enum
* 작성언어: typescript
* enum 변수는 `SCREAMING_SNAKE_CASE`로 작성한다.
```typescript
export namespace Enum {
  export namespace SAMPLE {
    export namespace ALBUM {
      export enum TEST {
        TEST_1 = 'TEST_1',
        TEST_2 = 'TEST_2'
      }
    }
  }

  export namespace CORE {
    export enum ENV {
      LOCAL = 'local',
      DEV = 'development',
      PROD = 'production'
    }

    export namespace STORAGE {
      export enum KEY {
        TEST = 'TEST'
      }

      export enum TYPE {
        STRING = 'string',
        NUMBER = 'number',
        BOOLEAN = 'boolean',
        OBJECT = 'object',
        ARRAY = 'array',
      }
    }

    export enum SPINNER {
      DEFAULT = 'DEFAULT',
      WRITE = 'WRITE',
      SPINNER_ERROR = 'SPINNER_ERROR'
    }

    export enum DATE_FORMAT {
      RETURN_DATE_TIME = 'YYYY-MM-DDTHH:mm',
      RETURN_DATE_TIME_SECOND = 'YYYY-MM-DDTHH:mm:ss',

      DISPLAY_DATE_TIME = 'YYYY-MM-DD HH:mm',
      DISPLAY_DATE_TIME_SECOND = 'YYYY-MM-DD HH:mm:ss'
    }
  }
}
```

## model
* 작성언어: typescript
* Request / Response - API 요청, 수신 기준으로 Request / Response로 분할하여 작성
* Request Validator는 `class-validator` 를 사용하여 작성
* Object, Array의 하위항목은 `@ValidateNested`를 사용하여 작성
* 필수항목은 `@IsNotEmpty`, 선택항목은 `@IsOptional`을 사용함
```typescript
import 'reflect-metadata';
import {Expose} from 'class-transformer';
import {IsNotEmpty, IsNumber, IsString} from 'class-validator';

import {Description} from '@/src/core/decorator/description.decorator';

export namespace Album {
  export namespace Request {
    export class Add {
      @Expose() @Description('사용자-아이디')
      @IsNumber() @IsNotEmpty()
      userId!: number;

      @Expose() @Description('제목')
      @IsString() @IsNotEmpty()
      title!: string;
    }
  }

  export namespace Response {
    export class FindAll {
      @Expose() @Description('사용자-아이디')
      userId!: number;

      @Expose() @Description('아이디')
      id!: number;

      @Expose() @Description('제목')
      title!: string;
    }

    export class FindOne {
      @Expose() @Description('사용자-아이디')
      userId!: number;

      @Expose() @Description('아이디')
      id!: number;

      @Expose() @Description('제목')
      title!: string;
    }
  }
}
```



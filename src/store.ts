import { createWrapper        } from 'next-redux-wrapper';

import { Action               } from '@reduxjs/toolkit';
import { ThunkAction          } from '@reduxjs/toolkit';
import { configureStore       } from '@reduxjs/toolkit';
import { getDefaultMiddleware } from '@reduxjs/toolkit';
import   errorSlice             from '@/app/shared/store/error.slice';
import   modalSlice             from '@/app/shared/store/modal.slice';
import   searchSlice            from '@/app/shared/store/search.slice';
import   spinnerSlice           from '@/app/shared/store/spinner.slice';
import   authResultSlice        from '@/app/shared/store/auth-result.slice';

const makeStore = () => {
  const middleware = getDefaultMiddleware();

  return configureStore({
    reducer : {
      errorSlice     : errorSlice.reducer,
      spinnerSlice   : spinnerSlice.reducer,
      modalSlice     : modalSlice.reducer,
      searchSlice    : searchSlice.reducer,
      authResultSlice: authResultSlice.reducer,
    },
    middleware,
    devTools: process.env.NODE_ENV === 'development',
  });
};

export type AppStore            = ReturnType<typeof makeStore>; // store 타입
export type RootState           = ReturnType<AppStore['getState']>; // RootState 타입
export type AppDispatch         = AppStore['dispatch']; // dispatch 타입
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action>; // Thunk 를 위한 타입

const wrapper = createWrapper<AppStore>(makeStore, {
  debug: process.env.NODE_ENV === 'development',
});

export default wrapper;

import { NextRequest  } from 'next/server';
import { NextResponse } from 'next/server';

import { CookieEnum   } from '@/app/shared/enum/cookie.enum';
import { MENU         } from '@/app/system/config/menu.config';

export async function middleware(req: NextRequest) {

  const PUBLIC_FILE        = /\.(.*)$/;
  const {origin, pathname} = req.nextUrl;

  if (!PUBLIC_FILE.test(pathname) && pathname !== '/auth/login') {
    const token: string   = req.cookies.get(CookieEnum.KEY.AUTH_TOKEN)!;
    const menus: string[] = !!req.cookies.get(CookieEnum.KEY.MENU) ? [...JSON.parse(req.cookies.get(CookieEnum.KEY.MENU)!)] : [];

    menus.push(...MENU);

    if (!token) {
      req.cookies.clear();

      return NextResponse.redirect(`${origin}/auth/login`);
    }

    if (!menus.some((menu: string) => pathname.indexOf(menu) >= 0)) {

      return NextResponse.redirect(`${origin}`);
    }

    return NextResponse.next();
  }
}

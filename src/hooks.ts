import type { TypedUseSelectorHook } from 'react-redux';
import      { useSelector          } from 'react-redux';
import      { useDispatch          } from 'react-redux';
import      { useMutation          } from 'react-query';

import        authResultSlice        from '@/app/shared/store/auth-result.slice';
import      { AuthEnum             } from '@/app/auth/enum/auth.enum';
import      { AuthApi              } from '@/pages/api/auth.api';
import      { Auth                 } from '@/app/auth/model/auth.model';
import type { RootState            } from './store';
import type { AppDispatch          } from './store';

export const useAppDispatch: () => AppDispatch = useDispatch;

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export const useAuthMutation = () => {

  const authApi: AuthApi = new AuthApi();
  const dispatch         = useAppDispatch();

  const useLoginMutation = () => useMutation(
    (loginFormData: Auth.Request.Login) => authApi.login(loginFormData),
    {
      onSuccess: () => {
        dispatch(authResultSlice.actions.setResult(AuthEnum.AUTH_RESULT.LOGIN));
      },
    },
  );

  const useGetMeMutation = () => useMutation(
    () => authApi.getMe(),
    {
      onSuccess: () => {
        dispatch(authResultSlice.actions.setResult(AuthEnum.AUTH_RESULT.ME));
      },
    },
  );

  const useGetMenuMutation = () => useMutation(
    () => authApi.getMenu(),
    {
      onSuccess: () => {
        dispatch(authResultSlice.actions.setResult(AuthEnum.AUTH_RESULT.MENU));
      },
    },
  );

  return { useLoginMutation, useGetMeMutation, useGetMenuMutation };
};

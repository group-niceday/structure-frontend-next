import { AxiosResponse } from 'axios';

import   URL_INFIX       from '@/app/shared/config/api.config';
import { Demo          } from '@/app/guide/model/demo.model';
import { Validate      } from '@/core/decorator/validate.decorator';
import { Inject        } from '@/core/decorator/tsyringe';
import   DemoAxios       from '@/core/axios/demo.axios';

export class DemoApi {
  @Inject() http!: DemoAxios;

	getPage(params: Demo.Request.Search) {

		return this.http
			.get(URL_INFIX.DEMO.concat('/page'), { params })
			.then((response: AxiosResponse<Demo.Response.FindAll[]>) => response.data);
	}

	getOne(id: number) {

		return this.http
			.get(URL_INFIX.DEMO.concat(`/${id}`))
			.then((response: AxiosResponse<Demo.Response.FindOne>) => response.data);
	}

	@Validate()
	async add(add: Demo.Request.Add) {

		await this.http
			.post(URL_INFIX.DEMO, add)
			.then((response: AxiosResponse<Demo.Response.FindOne>) => response.data);
	}

	@Validate()
	async modify(modify: Demo.Request.Modify) {

		await this.http
			.put(URL_INFIX.DEMO.concat(`/${modify.id}`), modify)
			.then((response: AxiosResponse<Demo.Response.FindOne>) => response.data);
	}

	delete(id: number) {

		return this.http
			.delete(URL_INFIX.DEMO.concat(`/${id}`))
			.then((response: AxiosResponse<Demo.Response.FindOne>) => response.data);
	}
}

import { AxiosResponse } from 'axios';

import   ApiAxios        from '@/core/axios/api.axios';
import   AuthAxios       from '@/core/axios/auth.axios';
import   AuthService     from '@/app/auth/service/auth.service';
import   CookieService   from '@/app/shared/service/cookie.service';
import   StorageService  from '@/app/shared/service/storage.service';
import   URL_INFIX       from '@/app/shared/config/api.config';
import { CookieEnum    } from '@/app/shared/enum/cookie.enum';
import { StorageEnum   } from '@/app/shared/enum/storage.enum';
import { Auth          } from '@/app/auth/model/auth.model';
import { Inject        } from '@/core/decorator/tsyringe';
import { Validate      } from '@/core/decorator/validate.decorator';

export class AuthApi {

  @Inject() http          !: ApiAxios;
  @Inject() httpAuth      !: AuthAxios;
  @Inject() authService !: AuthService;
  @Inject() cookieService !: CookieService;
  @Inject() storageService!: StorageService;

  @Validate()
  login(auth: Auth.Request.Login) {

    this.storageService.resetLocalStorage();

    const formData = new FormData();

    formData.append('grant_type', process.env.NEXT_PUBLIC_AUTH_GRANT_TYPE!);
    formData.append('client_id', process.env.NEXT_PUBLIC_AUTH_CLIENT_ID!);
    formData.append('username', auth.loginId);
    formData.append('password', auth.password);

    return this.httpAuth
      .post(URL_INFIX.OAUTH.concat('/token'), formData)
      .then((response: AxiosResponse<Auth.Response.Auth>) => {
        this.cookieService.setCookie(CookieEnum.KEY.AUTH_TOKEN, response.data);

        response.data;
      });
  }

  getMe() {

    return this.http
      .get(URL_INFIX.ACCOUNTS.concat('/me'))
      .then((response: AxiosResponse<Auth.Response.Me>) => {
        this.storageService.setLocalStorage(StorageEnum.KEY.ME, response.data);

        response.data;
      });
  }

  getMenu() {

    return this.http
      .get(URL_INFIX.MENUS.concat('/left'))
      .then((response: AxiosResponse<Auth.Response.Menu[]>) => {
        this.cookieService.setCookie(CookieEnum.KEY.MENU, this.authService.getLinks(response.data));

        response.data;
      });
  }
}

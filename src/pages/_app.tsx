import                                   'antd/dist/antd.css';
import                                   '@/assets/css/globals.css';
import        React                 from 'react';
import      { useEffect           } from 'react';
import      { useRouter           } from 'next/router';
import type { AppProps            } from 'next/app';
import      { QueryClient         } from 'react-query';
import      { DehydratedState     } from 'react-query';
import      { Hydrate             } from 'react-query';
import      { QueryClientProvider } from 'react-query';
import      { ReactQueryDevtools  } from 'react-query/devtools';
import      { appWithTranslation  } from 'next-i18next';
import      { Spin                } from 'antd';

import        Container             from '@/core/container';
import        CookieService         from '@/app/shared/service/cookie.service';
import        StorageService        from '@/app/shared/service/storage.service';
import        AppLayout             from '@/app/system/layout/app-layout';
import        wrapper               from '@/store';
import      { CookieEnum          } from '@/app/shared/enum/cookie.enum';
import      { useAppSelector      } from '@/hooks';

const MyApp = ({Component, pageProps}: AppProps<{ dehydratedState: DehydratedState }>) => {

  const queryClient                    = new QueryClient();
  const cookieService : CookieService  = Container.resolve<CookieService>(CookieService);
  const storageService: StorageService = Container.resolve<StorageService>(StorageService);
  const router                         = useRouter();
  const spinner                        = useAppSelector(state => state.spinnerSlice.spinner);

  useEffect(() => {

    if (router.route === '/auth/login') {
      cookieService.removeCookie(CookieEnum.KEY.AUTH_TOKEN);
      cookieService.removeCookie(CookieEnum.KEY.MENU);
      storageService.resetLocalStorage();
    }
  }, [router]);

  return (
    <QueryClientProvider client={queryClient} >
      <Hydrate state={pageProps.dehydratedState} >
        {router.route === '/auth/login' ? (
          <Spin tip="Loading..." spinning={spinner} >
            <Component {...pageProps} />
          </Spin>
        ) : (
          <AppLayout>
            <Spin tip="Loading..." spinning={spinner} >
              <Component {...pageProps} />
            </Spin>
          </AppLayout>
        )}
      </Hydrate>
      <ReactQueryDevtools/>
    </QueryClientProvider>
  );
};

export default appWithTranslation(wrapper.withRedux(MyApp));

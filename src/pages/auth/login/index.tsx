import   React                       from 'react';
import { ParsedUrlQuery            } from 'querystring';
import { PreviewData               } from 'next';
import { GetServerSideProps        } from 'next';
import { GetServerSidePropsContext } from 'next';
import { useEffect                 } from 'react';
import { useRouter                 } from 'next/router';
import { serverSideTranslations    } from 'next-i18next/serverSideTranslations';

import   Container                   from '@/core/container';
import   CookieService               from '@/app/shared/service/cookie.service';
import   StorageService              from '@/app/shared/service/storage.service';
import   Login                       from '@/app/auth/view/login';
import   wrapper                     from '@/store';
import { AuthEnum                  } from '@/app/auth/enum/auth.enum';
import { CookieEnum                } from '@/app/shared/enum/cookie.enum';
import { StorageEnum               } from '@/app/shared/enum/storage.enum';
import { Auth                      } from '@/app/auth/model/auth.model';
import { useAuthMutation           } from '@/hooks';
import { useAppSelector            } from '@/hooks';

type Props = {
  login: Auth.Request.Login;
};

const LoginPage = ({login}: Props) => {

  const cookieService : CookieService  = Container.resolve<CookieService>(CookieService);
  const storageService: StorageService = Container.resolve<StorageService>(StorageService);
  const router                         = useRouter();
  const { useLoginMutation      }      = useAuthMutation();
  const { useGetMeMutation      }      = useAuthMutation();
  const { useGetMenuMutation    }      = useAuthMutation();
  const { mutate: loginMutate   }      = useLoginMutation();
  const { mutate: getMeMutate   }      = useGetMeMutation();
  const { mutate: getMenuMutate }      = useGetMenuMutation();
  const result                         = useAppSelector(state => state.authResultSlice.result);

  const loginHandler = (loginFormData: Auth.Request.Login) => {

    if (loginFormData.rememberYn) {
      cookieService.setCookie(CookieEnum.KEY.REMEMBER_ID, loginFormData.loginId);
    } else {
      if (!!cookieService.getCookie(CookieEnum.KEY.REMEMBER_ID)) {
        cookieService.removeCookie(CookieEnum.KEY.REMEMBER_ID);
      }
    }

    loginMutate(loginFormData);
  };

  useEffect(() => {

    if (result === AuthEnum.AUTH_RESULT.LOGIN) {
      getMeMutate();
    } else if (result === AuthEnum.AUTH_RESULT.ME) {
      const me: Auth.Response.Me = storageService.getLocalStorage(StorageEnum.KEY.ME, StorageEnum.TYPE.OBJECT);

      if (!!me && !!me.roles && me.roles.length > 0) {
        getMenuMutate();
      }
    } else if (result === AuthEnum.AUTH_RESULT.MENU) {
      router.push('/');
    }
  }, [result]);

  return <Login login={login} onLogin={loginHandler} />;
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(

  () => async (context: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>) => {
    const login: Auth.Request.Login = new Auth.Request.Login();

    if (!!context.req.headers.cookie && context.req.headers.cookie.includes(CookieEnum.KEY.REMEMBER_ID)) {
      login.loginId    = 'henry';
      login.rememberYn = true;
    }

    return {
      props: {
        ...(await serverSideTranslations(context.locale as string, ['login'])),
        login: JSON.parse(JSON.stringify(login)),
      },
    };
  },
);

export default LoginPage;

import React          from 'react';

import { PageHeader } from 'antd';

import GuideCard from '@/app/guide/view/guide-card';

const Card = () => (
  <>
    <PageHeader title="CardGuide" />
    <GuideCard />
  </>
  );

export default Card;

import React          from 'react';
import { PageHeader } from 'antd';

import GuideForm      from '@/app/guide/view/guide-form';

const Form = () => (

  <>
    <PageHeader title="FormGuide" />
    <GuideForm />
  </>
    );

export default Form;

import React          from 'react';
import { PageHeader } from 'antd';

import GuideMap       from '@/app/guide/view/guide-map';

const Map = () => (

  <>
    <PageHeader title="MapGuide" />
    <GuideMap />
  </>
  );

export default Map;

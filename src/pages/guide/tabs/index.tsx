import React from 'react';
import { PageHeader } from 'antd';

import GuideTabs from '@/app/guide/view/guide-tabs';

const Tabs = () => (

  <>
    <PageHeader title="TabsGuide" />
    <GuideTabs />
  </>
  );

export default Tabs;

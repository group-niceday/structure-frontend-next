import React          from 'react';
import { PageHeader } from 'antd';

import GuideButton    from '@/app/guide/view/guide-button';

const Button = () => (

  <>
    <PageHeader title="ButtonGuide" />
    <GuideButton />
  </>
);

export default Button;

import React from 'react';
import { PageHeader } from 'antd';

import GuideUpload from '@/app/guide/view/guide-upload';

const Upload = () => (
  <>
    <PageHeader title="UploadGuide" />
    <GuideUpload />
  </>
    );

export default Upload;

import React          from 'react';
import { PageHeader } from 'antd';

import GuideModal     from '@/app/guide/view/guide-modal';

const Modal = () => (

  <>
    <PageHeader title="ModalGuide" />
    <GuideModal />
  </>
  );

export default Modal;

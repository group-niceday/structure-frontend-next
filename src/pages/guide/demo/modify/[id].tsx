import React                      from 'react';
import { useEffect              } from 'react';
import { GetServerSideProps     } from 'next';
import { useRouter              } from 'next/router';
import { useMutation            } from 'react-query';
import { useDispatch            } from 'react-redux';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { DemoApi                } from '@/pages/api/demo.api';
import { offSpinner             } from '@/app/shared/store/spinner.slice';
import { onSpinner              } from '@/app/shared/store/spinner.slice';
import { Demo }                   from '@/app/guide/model/demo.model';
import wrapper                    from '@/store';
import DemoModify                 from '@/app/guide/view/demo/demo-modify';

type Props = {
	id: number;
};

const DemoModifyPage = ({ id }: Props) => {

	const demoApi: DemoApi     = new DemoApi();
	const router               = useRouter();
	const dispatch             = useDispatch();
	const modify               = useMutation((demo: Demo.Request.Modify) => demoApi.modify(demo));

	const modifyAccountHandler = (demo: Demo.Request.Modify) => {

		modify.mutate(demo, {
			onSuccess: () => router.replace('/guide/demo'),
		});
	};

	useEffect(() => {

		if (modify.isLoading) {
			dispatch(onSpinner());
		}
		if (modify.isSuccess || modify.isError) {
			dispatch(offSpinner());
		}
	}, [modify.isLoading, modify.isSuccess, modify.isError]);

	return <DemoModify id={id} fnModifyAccount={modifyAccountHandler} />;
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(() => async (context: any) => ({

		props: {
			...(await serverSideTranslations(context.locale, ['common', 'demo'])),
			id: context.params.id,
		},
	}));

export default DemoModifyPage;

import React                      from 'react';
import { useEffect              } from 'react';
import { useRouter              } from 'next/router';
import { useDispatch            } from 'react-redux';
import { useMutation            } from 'react-query';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { offSpinner             } from '@/app/shared/store/spinner.slice';
import { onSpinner              } from '@/app/shared/store/spinner.slice';
import { DemoApi                } from '@/pages/api/demo.api';
import { Demo                   } from '@/app/guide/model/demo.model';
import DemoAdd                    from '@/app/guide/view/demo/demo-add';

const DemoAddPage = () => {

	const demoApi: DemoApi     = new DemoApi();
	const router               = useRouter();
	const dispatch             = useDispatch();
	const add                  = useMutation((demo: Demo.Request.Add) => demoApi.add(demo));

	const addAccountHandler = (demo: Demo.Request.Add) => {

		add.mutate(demo, {
			onSuccess: () => router.replace('/guide/demo'),
		});
	};

	useEffect(() => {

		if (add.isLoading) {
			dispatch(onSpinner());
		}
		if (add.isSuccess || add.isError) {
			dispatch(offSpinner());
		}
	}, [add.isLoading, add.isSuccess, add.isError]);

	return <DemoAdd fnAddAccount={addAccountHandler} />;
};

export const getStaticProps = async ({ locale }) => ({
	props: {
		...(await serverSideTranslations(locale, ['common', 'demo'])),
	},
});

export default DemoAddPage;

import   React                    from 'react';
import { GetServerSideProps     } from 'next';
import { useRouter              } from 'next/router';
import { QueryClient            } from 'react-query';
import { useMutation            } from 'react-query';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation         } from 'next-i18next';
import { Button                 } from 'antd';
import { Space                  } from 'antd';

import   NConfirm                 from '@/app/shared/components/n-confirm';
import   DemoDetail               from '@/app/guide/view/demo/demo-detail';
import   wrapper                  from '@/store';
import { ActionEnum             } from '@/app/shared/enum/action.enum';
import { DemoApi                } from '@/pages/api/demo.api';

type Props = {
	id: number;
};

const DemoDetailPage = ({ id }: Props) => {

	const demoApi: DemoApi     = new DemoApi();
	const router               = useRouter();
	const { t }                = useTranslation('common');
	const remove               = useMutation((removeId: number) => demoApi.delete(removeId));

	const fnRemove = () => {

		remove.mutate(id, {
			onSuccess: () => router.replace('/guide/demo'),
		});
	};

	const fnMoveModifyPage = () => {

		router.push(`/guide/demo/modify/${id}`);
	};

	return (
  <>
    <DemoDetail id={id} />

    <Space>
      <Button type="primary" onClick={fnMoveModifyPage} size="large">
        {t('button.modify')}
      </Button>
      <NConfirm submit={fnRemove} type={ActionEnum.RESULT.ENUM.DELETE} size="large" />
      <Button type="primary" onClick={() => router.back()} size="large">
        {t('button.list')}
      </Button>
    </Space>
  </>
	);
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(() => async (context: any) => {

	const queryClient        = new QueryClient();
	const demoApi: DemoApi   = new DemoApi();
	await queryClient.prefetchQuery(['demos', demoApi], () => demoApi.getOne(context.params.id));

	return {
		props: {
			...(await serverSideTranslations(context.locale, ['common', 'demo'])),
			id: context.params.id,
		},
	};
});

export default DemoDetailPage;

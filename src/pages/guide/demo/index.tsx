import React                      from 'react';
import { GetServerSideProps     } from 'next';
import { useRouter              } from 'next/router';
import { useTranslation         } from 'next-i18next';
import { dehydrate              } from 'react-query';
import { QueryClient            } from 'react-query';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Button                 } from 'antd';

import { DemoApi                } from '@/pages/api/demo.api';
import { Demo }                   from '@/app/guide/model/demo.model';
import wrapper                    from '@/store';
import DemoList                   from '@/app/guide/view/demo/demo-list';
import buttonStyle                from '@/assets/css/Button.module.css';

type Props = {
	searchProps: Demo.Request.Search;
};

const DemoListPage = ({ searchProps }: Props) => {

	const router = useRouter();
	const { t } = useTranslation(['common', 'demo']);

	const fnMoveAddPage = () => {
		router.push('/guide/demo/add');
	};

	return (
  <>
    <h1>{t('mainTitle', { ns: 'demo' })}</h1>
    <DemoList searchProps={searchProps} />

    <Button size="large" onClick={fnMoveAddPage} className={buttonStyle.primary}>
      {t('button.add')}
    </Button>
  </>
	);
};

export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(() => async ({ locale, query }) => {

	const queryClient          = new QueryClient();
	const demoApi: DemoApi     = new DemoApi();

	await queryClient.prefetchQuery(['demos', demoApi], () => demoApi.getPage(new Demo.Request.Search()));

	return {
		props: {
			...(await serverSideTranslations(locale!, ['common', 'demo'])),
			dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient))),
			searchProps: JSON.parse(JSON.stringify(query)),
		},
	};
});

export default DemoListPage;

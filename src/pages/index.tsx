import        React                    from 'react';
import type { NextPage               } from 'next';
import      { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import        DashboardContent         from '@/app/system/view/dashboard.content';

const Home: NextPage = () => (
    <DashboardContent />
	);

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});

export default Home;

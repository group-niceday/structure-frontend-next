import                     'reflect-metadata';
import { Type       } from 'class-transformer';

import { Attribute  } from '@/core/decorator/attribute.decorator';
import { IsString   } from '@/core/decorator/validator';
import { IsBoolean  } from '@/core/decorator/validator';
import { IsNotEmpty } from '@/core/decorator/validator';

export namespace Auth {
  export namespace Request {
    export class Login {
      @Attribute('로그인 아이디')
      @IsString() @IsNotEmpty()
      loginId!: string;

      @Attribute('비밀번호')
      @IsString() @IsNotEmpty()
      password!: string;

      @Attribute('아이디 저장 유무')
      @IsBoolean()
      rememberYn: boolean = false;
    }
  }

  export namespace Response {
    export class Auth {
      @Attribute('액세스 토큰')
      'access_token'!: string;

      @Attribute('리프레시 토큰')
      'refresh_token'!: string;

      @Attribute('토큰 타입')
      'token_type'!: string;

      @Attribute('토큰 만료 시간')
      'expires_in'!: number;

      @Attribute('범위')
      scope!: string;
    }

    export class Me {
      @Attribute('아이디')
      id!: number;

      @Attribute('로그인 아이디')
      loginId!: string;

      @Attribute('이름')
      name!: string;

      @Attribute('권한')
      @Type(() => Role)
      roles!: Role[];
    }

    export class Role {
      @Attribute('아이디')
      id!: number;

      @Attribute('이름')
      name!: string;

      @Attribute('권한 유형')
      roleType!: string;
    }

    export class Menu {
      @Attribute('아이디')
      id!: number;

      @Attribute('이름')
      name!: string;

      @Attribute('link')
      link!: string;

      @Attribute('순서')
      sort!: number;

      @Attribute('하위 메뉴')
      @Type(() => Menu)
      children!: Menu[];

      constructor(options?: { name?: string; link?: string; icon?: string; children?: Menu[]; }) {
        if (!!options) {
          if (!!options.name) {
            this.name = options.name;
          }

          if (!!options.link) {
            this.link = options.link;
          }

          if (!!options.children && options.children.length > 0) {
            this.children = options.children;
          }
        }
      }
    }
  }
}

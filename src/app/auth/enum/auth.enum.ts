export namespace AuthEnum {
	export enum AUTH_RESULT {
		LOGIN = 'LOGIN',
		ME    = 'ME',
		MENU  = 'MENU',
	}
}

import { Auth       } from '@/app/auth/model/auth.model';
import { Injectable } from '@/core/decorator/tsyringe';

@Injectable()
export default class AuthService {
  getLinks(menus: Auth.Response.Menu[]) {

    const links: string[] = [];

    menus.forEach((menu: Auth.Response.Menu) => {
      if (!!menu.children && menu.children.length > 0) {
        links.push(...this.getLinks(menu.children));
      } else {
        if (!!menu.link) {
          links.push(menu.link);
        }
      }
    });

    return links;
  }
}

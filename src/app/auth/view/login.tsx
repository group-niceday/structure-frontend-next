import   React            from 'react';
import   Link             from 'next/link';
import { useRouter      } from 'next/router';
import { useTranslation } from 'next-i18next';
import { Formik         } from 'formik';
import { Col            } from 'antd';
import { Row            } from 'antd';
import { Card           } from 'antd';
import { Form           } from 'antd';
import { Input          } from 'antd';
import { Space          } from 'antd';
import { Button         } from 'antd';
import { Select         } from 'antd';
import { Checkbox       } from 'antd';

import   classes          from '@/app/auth/view/login.module.css';
import   Container        from '@/core/container';
import   Mapper           from '@/core/service/mapper.service';
import   NInput           from '@/app/shared/components/form/n-input';
import { FormEnum       } from '@/app/shared/enum/form.enum';
import { Auth           } from '@/app/auth/model/auth.model';

type Props = {
  login  : Auth.Request.Login;
  onLogin: (values: Auth.Request.Login) => void;
};

const Login = ({onLogin, login}: Props) => {

  const mapper: Mapper = Container.resolve<Mapper>(Mapper);
  const router         = useRouter();
  const { t }          = useTranslation('login');

  const onSubmit = async (values: Auth.Request.Login) => {

    onLogin(mapper.toObject(Auth.Request.Login, values));
  };

  const handleLocale = (value: string) => {

    router.push('/auth/login', '/auth/login', {locale: value});
  };

  return (
    <Space size="large"
           align="center"
           direction="vertical"
           style={{display: 'flex'}}
           className={classes.loginForm} >
      <Card>
        <h1 className={classes.logo} >
          <img src="/logo.png" alt="logo" />
        </h1>
        <Row className={classes.loginHeader} >
          <Col className={classes.loginH1} >
            {t('title')}
          </Col>
          <Col>
            <Select defaultValue={router.locale}
                    onChange={handleLocale}
                    options={[
                      {
                        value: 'ko',
                        label: 'Korean',
                      },
                      {
                        value: 'en',
                        label: 'English',
                      },
                    ]} />
          </Col>
        </Row>

        <Formik initialValues={login} onSubmit={onSubmit} >
          {({ values, handleSubmit, handleChange}) =>
            (((values = mapper.toObject(Auth.Request.Login, values)),
            <Form layout="vertical"
                  autoComplete="off"
                  labelCol={{span: 8}}
                  wrapperCol={{span: 24}}
                  initialValues={login}
                  onFinish={handleSubmit} >
              <Form.Item label="ID" name="loginId" >
                <NInput type={FormEnum.INPUT_TYPE.TEXT}
                        name="loginId"
                        parent={values}
                        value={values.loginId}
                        placeholder={t('idPlaceholder')}
                        onChange={handleChange} />
              </Form.Item>

              <Form.Item label="PASSWORD" name="password" >
                <Input.Password name="password"
                                value={values.password}
                                placeholder={t('pwPlaceholder')}
                                onChange={handleChange} />
              </Form.Item>

              <Row className={classes.loginFormMid} >
                <Col span={12} >
                  <Form.Item name="rememberYn" valuePropName="checked" >
                    <Checkbox value={values.rememberYn} name="rememberYn" onChange={handleChange} >
                      {t('rememberId')}
                    </Checkbox>
                  </Form.Item>
                </Col>
                <Col span={12} className={classes.passwordForgot} >
                  <Link href="/auth/login" >
                    {t('forgotPw')}
                  </Link>
                </Col>
              </Row>

              <Form.Item wrapperCol={{span: 24}} className={classes.loginButton} >
                <Button type="primary" htmlType="submit" >
                  {t('loginButton')}
                </Button>
              </Form.Item>
            </Form>
          ))}
        </Formik>

        <div className={classes.partnerLink} >
          <Link href="/auth/login" >
            {t('signUp')}
          </Link>
        </div>
      </Card>
    </Space>
  );
};

export default Login;

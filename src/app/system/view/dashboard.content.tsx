import   React            from 'react';
import { useTranslation } from 'next-i18next';

const DashboardContent = () => {

  const { t } = useTranslation('common');

  return (
		<div>
      <h1>{t('title')}</h1>
		</div>
	);
};

export default DashboardContent;

import   React       from 'react';
import { useRouter } from 'next/router';
import { Menu      } from 'antd';
import { Layout    } from 'antd';

const { Sider } = Layout;

const menuItems = [
	{
		key: 1,
		label: '대시보드',
		children: [
			{
				key: 'dashboard',
				label: '대시보드',
				url: '/',
			},
		],
	},
	{
		key: 2,
		label: 'Guide',
		children: [
      {
        key: 'demo',
        label: 'Demo',
        url: '/guide/demo',
      },
      {
        key: 3,
        label: 'Component',
        children: [
          {
            key: 'form',
            label: 'Form',
            url: '/guide/form',
          },
          {
            key: 'button',
            label: 'Button',
            url: '/guide/button',
          },
          {
            key: 'card',
            label: 'Card',
            url: '/guide/card',
          },
          {
            key: 'modal',
            label: 'Modal',
            url: '/guide/modal',
          },
          {
            key: 'map',
            label: 'Map',
            url: '/guide/map',
          },
          {
            key: 'tabs',
            label: 'Taps',
            url: '/guide/tabs',
          },
          {
            key: 'upload',
            label: 'Upload',
            url: '/guide/upload',
          },
        ],
      },
		],
	},
];

const AppLeftMenu = () => {

	const router = useRouter();

	const moveContent = (item: any) => {

		router.push(`${item.item.props.url}`);
	};

	return (
    <Sider width={200} className="site-layout-background" >
      <Menu theme="dark"
            mode="inline"
            defaultSelectedKeys={['1']}
            items={menuItems}
            onClick={moveContent}
            style={{ height: '100%', borderRight: 0 }} />
    </Sider>
	);
};

export default AppLeftMenu;

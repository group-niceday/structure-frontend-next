import   React       from 'react';
import { Layout }    from 'antd';

import   classes     from '@/assets/css/Header.module.css';
import   AppLeftMenu from '@/app/system/view/app-left-menu';

const { Header, Content } = Layout;

type Props = {
	children: JSX.Element;
};

const AppLayout = ({ children }: Props) => (
		<Layout>
			<Header className={classes.header} >
				<div className="logo" >로고</div>
			</Header>
			<Layout className="site-layout" >
				<AppLeftMenu />
				<Layout>
					<Content className="site-layout-background" style={{margin: '24px 16px', padding: 24, minHeight: 280}} >
						<main>{children}</main>
					</Content>
				</Layout>
			</Layout>
		</Layout>
	);

export default AppLayout;

import   React        from 'react';
import { Button     } from 'antd';
import { Popconfirm } from 'antd';
import { SizeType   } from 'antd/es/config-provider/SizeContext';

import   buttonStyle  from '@/assets/css/Button.module.css';
import { ActionEnum } from '@/app/shared/enum/action.enum';

type Props = {
  submit?: any;
  values?: object;
  type?  : string;
  size?  : SizeType;
};

const NConfirm = ({ submit, values, type, size }: Props) => (

  <>
    {type === ActionEnum.RESULT.ENUM.ADD && (
    <Popconfirm
      title="add"
      onConfirm={() => { submit(values); }}
    >
      <Button size={size} className={buttonStyle.primary}>
        add
      </Button>
    </Popconfirm>
    )}
    {type === ActionEnum.RESULT.ENUM.UPDATE && (
    <Popconfirm
      title="update"
      onConfirm={() => { submit(values); }}
    >
      <Button size={size} className={buttonStyle.secondary}>update</Button>
    </Popconfirm>
    )}
    {type === ActionEnum.RESULT.ENUM.DELETE && (
    <Popconfirm
      title="delete"
      onConfirm={() => { submit(); }}
    >
      <Button size={size} className={buttonStyle.danger}>delete</Button>
    </Popconfirm>
    )}
  </>
);

export default NConfirm;

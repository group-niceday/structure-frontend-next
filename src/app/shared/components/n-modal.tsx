import   React            from 'react';
import { Modal          } from 'antd';

import { useAppSelector } from '@/hooks';

type Props = {
	title   : string;
	width   : number;
	children: any;
};

const NModal = ({ title, children, width }: Props) => {
	const isModal = useAppSelector(state => state.modalSlice.isModal);

	return (
    <Modal title={title}
           open={isModal}
           footer={null}
           width={width} >
      {children}
    </Modal>
	);
};

export default NModal;

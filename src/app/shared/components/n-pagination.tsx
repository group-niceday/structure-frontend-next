import   React        from 'react';
import { useRouter  } from 'next/router';
import { Pagination } from 'antd';

import   classes      from '@/app/shared/components/n-pagination.module.css';

type Props = {
	total      : number | undefined;
	path       : string;
	searchKey  : string;
	searchValue: string | string[] | undefined;
};

const NPagination = ({ total, path, searchKey, searchValue }: Props) => {
	const router = useRouter();

	return (
		<Pagination
			className={classes.pagination}
			total={total ?? 1}
			showTotal={(total, range) => `${range[0]}-${range[1]} of ${total}`}
			defaultCurrent={1}
			defaultPageSize={10}
			current={!!router.query.page ? Number(router.query.page) + 1 : 1}
			onChange={(current, pageSize) => {
				const obj = {};
				obj[searchKey] = searchValue;

				if (obj[searchKey]?.length > 0) {
					router.push({
						pathname: path,
						query: {
							page: current - 1,
							size: pageSize,
							...obj,
						},
					});
				} else {
					router.push({
						pathname: path,
						query: {
							page: current - 1,
							size: pageSize,
						},
					});
				}
			}}
			showSizeChanger
		/>
	);
};

export default NPagination;

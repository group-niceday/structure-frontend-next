import   React       from 'react';
import { Select }    from 'antd';

import   UtilService from '@/app/shared/service/util.service';

type Props = {
	value?        : string;
	setFieldValue?: any;
	placeholder?  : string;
	enumType?     : object;
  label?        : string;
};

const NEnumSelect = ({ value, setFieldValue, placeholder, enumType, label }: Props) => {

	const util = new UtilService();

	return (
  <Select placeholder={placeholder}
          value={value}
          onChange={(item: string) => {
				    setFieldValue(label, item);
		    	}}
  >
    {util.enumSelectHelper(enumType).map((item: string[]) => (
      <Select.Option value={item[0]} key={item[0]}>
        <div>{item[1]}</div>
      </Select.Option>
    ))}
  </Select>
	);
};

export default NEnumSelect;

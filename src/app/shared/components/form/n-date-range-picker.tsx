import React          from 'react';
import { format     } from 'date-fns';
import { ko         } from 'date-fns/locale';
import { DatePicker } from 'antd';

type Props = {
  name: string,
  value: string[],
  setFieldValue: any,
}

const NDateRangePicker = ({ name, value, setFieldValue }: Props) => (

    <DatePicker.RangePicker onChange={(value: any) => {

      const date1 = new Date(value[0]);
      const date2 = new Date(value[1]);

      setFieldValue(name, [format(date1, 'yyyy-MM-dd', { locale: ko }), format(date2, 'yyyy-MM-dd', { locale: ko })]);
    }}/>
  );

export default NDateRangePicker;

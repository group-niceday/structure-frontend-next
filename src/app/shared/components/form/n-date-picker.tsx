import React          from 'react';
import { format     } from 'date-fns';
import { ko         } from 'date-fns/locale';
import { DatePicker } from 'antd';

type Props = {
  name: string
  value: any,
  setFieldValue: any,
}

const NDatePicker = ({name, value, setFieldValue}: Props) => (

    <DatePicker onChange={(value: any) => {
      const date = new Date(value);

      setFieldValue(name, format(date, 'yyyy-MM-dd', { locale:ko }));
    }}/>
    );

export default NDatePicker;

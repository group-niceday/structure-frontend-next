import React      from 'react';

import { Switch } from 'antd';

type Props = {
  checkedLabel: string;
  unCheckedLabel: string;
  initial: boolean;
  setFieldValue: any;
  field: string
}

const NSwitch = ({ checkedLabel, unCheckedLabel, initial, setFieldValue, field }: Props) => (

  <Switch checkedChildren={checkedLabel}
          unCheckedChildren={unCheckedLabel}
          defaultChecked={initial}
          onChange={value => {
            setFieldValue(field, value);
          }} />
  );

export default NSwitch;

import   React        from 'react';
import { ReactNode }  from 'react';
import { useState  }  from 'react';
import { useEffect }  from 'react';
import { Input     }  from 'antd';

import   Container    from '@/core/container';
import   InputService from '@/app/shared/service/input.service';
import { FormEnum  }  from '@/app/shared/enum/form.enum';

type Props = {
  value?       : string | number;
  defaultValue?: string | number;
  name?        : string;
  placeholder? : string;
  maxLength?   : number;
  disabled?    : boolean;
  parent?      : any;
  size?        : FormEnum.INPUT_SIZE;
  type?        : FormEnum.INPUT_TYPE;
  prefix?      : ReactNode | null;
  suffix?      : ReactNode | null;
  onChange?    : any;
};

const NInput = ({
  value,
  defaultValue,
  name,
  type,
  size     = FormEnum.INPUT_SIZE.MIDDLE,
  placeholder,
  maxLength,
  disabled = false,
  parent,
  prefix   = null,
  suffix   = null,
  onChange,
}: Props) => {

  const inputService: InputService = Container.resolve<InputService>(InputService);
  const [isChange, setIsChange]    = useState(false);
  const [status,   setStatus  ]    = useState(FormEnum.STATUS.DEFAULT);

  const getState = () => {

    if (!!parent && !!name) {
      !inputService.getState(name, parent) ? setStatus(FormEnum.STATUS.ERROR) : setStatus(FormEnum.STATUS.DEFAULT);
    }
  };

  const getMessage = (): string => {

    let returnValue: string = '';

    if (!!parent && !!name) {
      returnValue = inputService.getMessage(name, parent);
    }

    return returnValue;
  };

  const handleChange = e => {

    setIsChange(true);
    onChange(e);
  };

  useEffect(() => {

    if (isChange) {
      getState();
    }
  }, [value]);

  return (
    <div>
      <Input value={value}
             defaultValue={defaultValue}
             name={name}
             type={type}
             size={size}
             placeholder={placeholder}
             status={status}
             maxLength={maxLength}
             disabled={disabled}
             prefix={prefix}
             suffix={suffix}
             onChange={handleChange} />
      <span>{getMessage()}</span>
    </div>
  );
};

export default NInput;

import React, { useEffect, useState } from 'react';

import { Input }                      from 'antd';

import InputService                   from '@/app/shared/service/input.service';
import { FormEnum }                   from '@/app/shared/enum/form.enum';

type Props = {
  rows?: number;
  placeholder?: string;
  name?: string;
  parent?: any;
  value?: string
  onChange?: any;
  maxLength?: number;
}

const NTextArea = ({ rows, placeholder, name, parent, value, onChange, maxLength } : Props) => {

  const [isChange, setIsChange]    = useState(false);
  const [status, setStatus]        = useState(FormEnum.STATUS.DEFAULT);

  const inputService: InputService = new InputService();

  const getState = () => {
    if (!!parent && !!name) {
      !inputService.getState(name, parent) ? setStatus(FormEnum.STATUS.ERROR) : setStatus(FormEnum.STATUS.DEFAULT);
    }
  };

  const getMessage = (): string => {
    let returnValue = '';

    if (!!parent && !!name) {
      returnValue = inputService.getMessage(name, parent);
    }

    return returnValue;
  };

  const handleChange = e => {
    setIsChange(true);
    onChange(e);
  };

  useEffect(() => {
    if (isChange) {
      getState();
    }
  }, [value]);

  return (
    <>
      <Input.TextArea value={value}
                      status={status}
                      name={name}
                      rows={rows}
                      placeholder={placeholder}
                      maxLength={maxLength}
                      showCount
                      onChange={handleChange} />
      <span>{getMessage()}</span>
    </>
    );
};

export default NTextArea;

export namespace CookieEnum {
	export enum KEY {
		AUTH_TOKEN  = 'AUTH_TOKEN',
		MENU        = 'MENU',
    REMEMBER_ID = 'REMEMBER_ID',
	}

	export enum TYPE {
		STRING  = 'string',
		NUMBER  = 'number',
		BOOLEAN = 'boolean',
		OBJECT  = 'object',
		ARRAY   = 'array',
	}
}

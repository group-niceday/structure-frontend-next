export namespace NotificationEnum {
  export enum TYPE {
    success = 'success',
    warning = 'warning',
    info    = 'info',
    error   = 'error'
  }

  export enum PLACEMENT {
    top         = 'top',
    topLeft     = 'topLeft',
    topRight    = 'topRight',
    bottom      = 'bottom',
    bottomLeft  = 'bottomLeft',
    bottomRight = 'bottomRight'
  }
}

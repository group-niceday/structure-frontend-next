export namespace ActionEnum {
	export namespace RESULT {
    export enum ENUM {
      ADD      = 'ADD',
      UPDATE   = 'UPDATE',
      DELETE   = 'DELETE',
      REQUEST  = 'REQUEST',
      APPLY    = 'APPLY',
      APPROVAL = 'APPROVAL',
      REJECT   = 'REJECT',
      CANCEL   = 'CANCEL',
      CHANGE   = 'CHANGE',
    }

    export enum NAME {
      ADD      = '등록',
      UPDATE   = '수정',
      DELETE   = '삭제',
      REQUEST  = '요청',
      APPLY    = '적용',
      APPROVAL = '승인',
      REJECT   = '반려',
      CANCEL   = '취소',
      CHANGE   = '변경',
    }
	}
}

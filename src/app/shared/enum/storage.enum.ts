export namespace StorageEnum {
	export enum KEY {
		ME         = 'ME',
		AUTH_TOKEN = 'AUTH_TOKEN',
		MENU       = 'MENU',
	}

	export enum TYPE {
		STRING  = 'string',
		NUMBER  = 'number',
		BOOLEAN = 'boolean',
		OBJECT  = 'object',
		ARRAY   = 'array',
	}
}

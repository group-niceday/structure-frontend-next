const URL_INFIX = {
  OAUTH   : '/oauth',
  ACCOUNTS: '/accounts',
  MENUS   : '/menus',
	DEMO    : '/artists',
};

export default URL_INFIX;

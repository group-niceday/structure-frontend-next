const MESSAGE = {
  CONFIRM: {
    ADD     : '등록 하시겠습니까?',
    UPDATE  : '수정 하시겠습니까?',
    DELETE  : '삭제 하시겠습니까?',
    REQUEST : '요청 하시겠습니까?',
    APPLY   : '적용 하시겠습니까?',
    APPROVAL: '승인 하시겠습니까?',
    REJECT  : '반려 하시겠습니까?',
    CANCEL  : '취소 하시겠습니까?',
  },

  RESULT_TITLE: {
    ADD     : '등록 완료',
    UPDATE  : '수정 완료',
    DELETE  : '삭제 완료',
    REQUEST : '요청 완료',
    APPLY   : '적용 완료',
    APPROVAL: '승인 완료',
    REJECT  : '반려 완료',
    CANCEL  : '취소 완료',
    CHANGE  : '변경 완료',
  },

  RESULT_MESSAGE: {
    ADD     : '등록 되었습니다.',
    UPDATE  : '수정 되었습니다.',
    DELETE  : '삭제 되었습니다.',
    REQUEST : '요청 되었습니다.',
    APPLY   : '적용 되었습니다.',
    APPROVAL: '승인 되었습니다.',
    REJECT  : '반려 되었습니다.',
    CANCEL  : '취소 되었습니다.',
    CHANGE  : '변경 되었습니다.',
  },

  ERROR: {
    DEFAULT       : '요청작업 처리중 오류가 발생 하였습니다.',
    TOKEN_TITLE   : '토큰 처리중 오류가 발생 하였습니다.',
    TOKEN_CONTENTS: '토큰이 만료되었습니다. 다시 로그인해 주세요.',
    MENU_ACCESS   : '해당 메뉴에 접근할수 없습니다.',
    CHECK_ADMIN   : '관리자에게 문의해 주세요.',
  },

  WARNING: {
    DUPLICATE     : '중복되는 항목이 있습니다.',
    NO_ACCOUNT    : '선택된 사용자가 없습니다.',
    NO_TEAM       : '선택된 팀이 없습니다.',
    CHECK_PASSWORD: '비밀번호 확인을 정확히 입력해 주세요.',
    FILE_LIMIT    : '해당파일의 크기가 너무 큽니다. (10M 이하)',
    FILE_EXT_LIST : '지정된 파일만 업로드 가능합니다. (jpeg, jpg, png, gif, zip, txt, pdf, ppt, pptx, xls, xlsx, doc, docx)',
  },

  NOTIFY: {
    TITLE: (status: string) => `Http Status: ${status}`,
    ERROR: (message: string) => `Error: ${message}`,
  },

  VALIDATOR: {
    isBoolean     : '자료형이 참거짓 형태여야 합니다.',
    isString      : '자료형이 문자 형태여야 합니다.',
    isInt         : '자료형이 정수 형태여야 합니다.',
    isNumberString: '자료형이 정수 형태여야 합니다.',
    isNotEmpty    : '필수 정보 입니다.',
    isArray       : '자료형이 배열 형태여야 합니다.',
    arrayNotEmpty : '배열에 데이터가 누락 되었습니다.',
    minLength     : '최소 입력 범위를 초과 하였습니다.',
    maxLength     : '최대 입력 범위를 초과 하였습니다.',
    length        : '입력 범위를 누락 되었습니다.',
    isHexColor    : '색상 정보를 올바르게 입력해 주세요.',
    max           : '최대 정수값을 넘을수 없습니다.',
    min           : '최소 정수값을 넘을수 없습니다.',
    isDateString  : '날짜 형식이어야 합니다.',
  },

  SERVER: {
    /**
     * E0001xxxx : 공통
     */
    E00010000: '오류가 발생하였습니다.',
    E00010001: '유효성 검사에 실패하였습니다.',
    E00010002: '해당 정보가 없습니다.',
    E00010003: '해당 요청은 더이상 불가능합니다.',
    E00010004: '해당 요청을 수락 할 수 없습니다.',
    E00010005: '이미 사용되고 있는 정보입니다.',
    E00010006: '인증오류가 발생하였습니다.',
    E00010007: '권한이 불충분합니다.',
    E00010008: '구현되지 않은 기능입니다.',
    E00010101: '비정상적인 접근입니다.',

    /**
     * E0002xxxx : 공통 - 파일
     */
    E00020000: '처리할 수 없는 타입의 파일입니다.',
    E00020001: '10MB 이상의 파일을 등록할 수 없습니다.',

    /**
     * E001001xx : 시스템 연동 - ant media
     */
    E00100100: '연동 시 오류가 발생하였습니다.',
    E00100101: '연동 시 5xx 오류가 발생하였습니다.',
    E00100102: '연동 시 4xx 오류가 발생하였습니다.',
    E00100103: '연동 시 요청한 서비스를 현재 처리할 수 없습니다.',
  },
};

export default MESSAGE;

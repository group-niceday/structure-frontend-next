import   MESSAGE        from '@/app/shared/config/message.config';
import { Notification } from '@/app/shared/model/notification.model';

const ERROR_MESSAGE = {

  ERROR       : (status: any): Notification.Message =>
    new Notification.Message({
      message    : MESSAGE.NOTIFY.TITLE(status),
      description: [MESSAGE.ERROR.DEFAULT, MESSAGE.NOTIFY.ERROR(status)],
    }),
  ERROR_CODE  : (status: any, code: string): Notification.Message =>
    new Notification.Message({
      message    : MESSAGE.NOTIFY.TITLE(status),
      description: [`${code}: ${!!MESSAGE.SERVER[code] ? MESSAGE.SERVER[code] : MESSAGE.ERROR.DEFAULT}`],
    }),
  ERROR_500   : (status: any): Notification.Message =>
    new Notification.Message({
      message    : MESSAGE.NOTIFY.TITLE(status),
      description: [MESSAGE.ERROR.DEFAULT, MESSAGE.ERROR.CHECK_ADMIN],
    }),
  ERROR_LOGIN : (): Notification.Message =>
    new Notification.Message({
      message    : '오류 메시지',
      description: ['로그인 정보를 다시 확인해 주세요.'],
    }),
  UNAUTHORIZED: (): Notification.Message =>
    new Notification.Message({
      message    : MESSAGE.NOTIFY.TITLE('unauthorized'),
      description: [MESSAGE.ERROR.TOKEN_TITLE, MESSAGE.NOTIFY.ERROR(MESSAGE.ERROR.TOKEN_CONTENTS)],
    }),
};

export default ERROR_MESSAGE;

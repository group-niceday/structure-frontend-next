export const FILE_LIMIT: number = 10 * 1024 * 1024;

export const FILE_EXT_LIST: string[] = [
  'JPEG',
  'JPG',
  'PNG',
  'GIF',
  'ZIP',
  'TXT',
  'PDF',
  'PPT',
  'PPTX',
  'XLS',
  'XLSX',
  'DOC',
  'DOCX',
];

export const FILE_EXT_ICON = [
  {
    exts : ['JPEG', 'JPG', 'PNG', 'GIF'],
    class: 'fa-regular fa-file-image',
  },
  {
    exts : ['ZIP'],
    class: 'fa-regular fa-file-zipper',
  },
  {
    exts : ['TXT'],
    class: 'fa-regular fa-file-lines',
  },
  {
    exts : ['PDF'],
    class: 'fa-regular fa-file-pdf',
  },
  {
    exts : ['PPT', 'PPTX'],
    class: 'fa-regular fa-file-powerpoint',
  },
  {
    exts : ['XLS', 'XLSX'],
    class: 'fa-regular fa-file-excel',
  },
  {
    exts : ['DOC', 'DOCX'],
    class: 'fa-regular fa-file-word',
  },
];

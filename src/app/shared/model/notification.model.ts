import                           'reflect-metadata';

import { NotificationEnum } from '@/app/shared/enum/notification.enum';

export namespace Notification {
  export class Message {
    message!    : string;
    description!: string[];
    type        : NotificationEnum.TYPE      = NotificationEnum.TYPE.warning;
    placement   : NotificationEnum.PLACEMENT = NotificationEnum.PLACEMENT.bottomRight;

    constructor(options?: { type?: NotificationEnum.TYPE; message?: string; description?: string[] }) {
      if (!!options) {
        if (!!options.type) {
          this.type = options.type;
        }

        if (!!options.message) {
          this.message = options.message;
        }

        if (!!options.description) {
          this.description = options.description;
        }
      }
    }
  }
}

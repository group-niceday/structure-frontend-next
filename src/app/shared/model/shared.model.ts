import { Attribute } from '@/core/decorator/attribute.decorator';

export namespace Shared {
  export namespace Response {
    export class Author {
      @Attribute('등록일시')
      createdAt!: string;

      @Attribute('등록자')
      createdBy!: string;

      @Attribute('수정일시')
      updatedAt!: string;

      @Attribute('수정자')
      updatedBy!: string;
    }
  }

  export class Options {
    @Attribute('OPTIONS_KEY')
    value!: string | number | boolean | null;

    @Attribute('OPTIONS_VALUE')
    text!: string;

    @Attribute('OPTIONS_DISABLED')
    disabled?: boolean = false;

    constructor(text?: string, value?: string | number | boolean | null, disabled?: boolean) {
      if (!!text) {
        this.text = text;
      }

      if (value !== null && value !== undefined) {
        this.value = value;
      }

      if (disabled !== null && disabled !== undefined) {
        this.disabled = disabled;
      }
    }
  }
}

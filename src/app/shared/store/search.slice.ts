import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	search: '',
};

const searchSlice = createSlice({
	name    : 'searchSlice',
	initialState,
	reducers: {
		setValue: (state, action) => {
			state.search = action.payload;
		},
	},
});

export const { setValue } = searchSlice.actions;

export default searchSlice;

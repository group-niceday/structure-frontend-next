import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	spinner: false as boolean,
};

const spinnerSlice = createSlice({

	name    : 'spinnerSlice',
	initialState,
	reducers: {
		onSpinner : state => {
			state.spinner = true;
		},
		offSpinner: state => {
			state.spinner = false;
		},
	},
});

export const { onSpinner, offSpinner } = spinnerSlice.actions;

export default spinnerSlice;

import { WritableDraft } from 'immer/dist/types/types-external';

import { AuthEnum      } from '@/app/auth/enum/auth.enum';
import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice   } from '@reduxjs/toolkit';

export interface AuthResultState {
  result: AuthEnum.AUTH_RESULT | null;
}

const initialState: AuthResultState = {
  result: null,
};

const authResultSlice = createSlice({
  name    : 'authResultSlice',
  initialState,
  reducers: {
    setResult: (state: WritableDraft<AuthResultState>, action: PayloadAction<AuthEnum.AUTH_RESULT | null>) => {
      state.result = action.payload;
    },
  },
});

export default authResultSlice;

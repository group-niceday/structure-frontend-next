import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	isModal: false as boolean,
};

export const modalSlice = createSlice({
	name    : 'modalSlice',
	initialState,
	reducers: {
		setModal: state => {
			state.isModal = !state.isModal;
		},
	},
});

export const { setModal } = modalSlice.actions;

export default modalSlice;

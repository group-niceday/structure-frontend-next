import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	error: {},
};

const errorSlice = createSlice({
	name    : 'errorSlice',
	initialState,
	reducers: {
		setError: (state, action) => {
			state.error = action.payload;
		},
	},
});

export const { setError } = errorSlice.actions;

export default errorSlice;

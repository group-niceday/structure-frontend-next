import   Cookies            from 'js-cookie';
import { ClassConstructor } from 'class-transformer';

import   Mapper             from '@/core/service/mapper.service';
import { CookieEnum       } from '@/app/shared/enum/cookie.enum';
import { Inject           } from '@/core/decorator/tsyringe';
import { Injectable       } from '@/core/decorator/tsyringe';

@Injectable()
export default class CookieService {

  @Inject() mapper!: Mapper;

	setCookie(key: CookieEnum.KEY, value: any, options?: any) {

		const cookie: any = typeof value === CookieEnum.TYPE.OBJECT ? JSON.stringify(value) : value;

		!!options ? Cookies.set(key, cookie, options) : Cookies.set(key, cookie);
	}

	getCookie<T>(key: CookieEnum.KEY, type?: CookieEnum.TYPE, classType?: ClassConstructor<T>) {

		let   returnValue: any = null;
    const cookie     : any = Cookies.get(key);

		if (!!cookie) {
      switch (type) {
        case CookieEnum.TYPE.STRING:
          returnValue = cookie;
          break;

        case CookieEnum.TYPE.NUMBER:
          returnValue = parseInt(cookie, 10);
          break;

        case CookieEnum.TYPE.BOOLEAN:
          returnValue = cookie !== 'false';
          break;

        case CookieEnum.TYPE.OBJECT:
          returnValue = !!classType ? this.mapper.toObject<T>(classType, JSON.parse(cookie)) : JSON.parse(cookie);
          break;

        case CookieEnum.TYPE.ARRAY:
          returnValue = !!classType ? this.mapper.toArray(classType, JSON.parse(cookie)) : JSON.parse(cookie);
          break;

        default:
          returnValue = cookie;
          break;
      }
    }

		return returnValue;
	}

	getAllCookies() {

		return Cookies.get();
	}

	removeCookie(key: CookieEnum.KEY, path?: any) {

		!!path ? Cookies.remove(key, { path: path }) : Cookies.remove(key);
	}
}

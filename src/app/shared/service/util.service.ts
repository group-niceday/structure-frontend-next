import   _            from 'lodash';

import { Injectable } from '@/core/decorator/tsyringe';

@Injectable()
export default class UtilService {

  safeCall(parent: any, key: string) {

    return _.get(parent, key);
  }

  // dateToFormatDate(value: Date | string, dateFormat: Enum.CORE.FORM.DATE_FORMAT): string {
  //
  //   const date: Date = new Date(value);
  //
  //   return format(date, this.enumService.getValue(Enum.CORE.FORM.DATE_FORMAT, dateFormat), {locale: ko});
  // }

  // dateToFormatTime(value: Date | string, timeFormat: Enum.CORE.FORM.TIME_FORMAT): string {
  //
  //   const date: Date = new Date(value);
  //
  //   return format(date, this.enumService.getValue(Enum.CORE.FORM.TIME_FORMAT, timeFormat), {locale: ko});
  // }

	isEmpty(checker: any): boolean {

		let flag = false;

		if (_.isPlainObject(checker)) {
			_.values(checker).forEach(value => {
				if (this.isEmpty(value)) {
					flag = true;
				}
			});
		} else if (_.isArray(checker)) {
			if (checker.length > 0) {
				checker.forEach((value: any) => {
					if (this.isEmpty(value)) {
						flag = true;
					}
				});
			} else {
				flag = true;
			}
		} else if (_.isNumber(checker)) {
			if (_.isNaN(checker) || _.isNull(checker)) {
				flag = true;
			}
		} else if (_.isString(checker)) {
			if (_.isEmpty(checker)) {
				flag = true;
			}
		} else if (_.isBoolean(checker)) {
			if (_.isNull(checker)) {
				flag = true;
			}
		} else {
			if (_.isEmpty(checker)) {
				flag = true;
			}
		}

		return flag;
	}

	enumSelectHelper(obj: any) {

		const result: any[] = Object.entries(obj);

		return result;
	}

	enumDetailHelper(detail: string, enumType) {

		const enums  = Object.entries(enumType);
		let   result = '';

		enums.forEach((obj: any) => {
			if (obj[0] === detail) {
				result = obj[1].toString();
			}
		});

		return result;
	}
}

import { ClassConstructor } from 'class-transformer';

import   Mapper             from '@/core/service/mapper.service';
import { StorageEnum }      from '@/app/shared/enum/storage.enum';
import { Inject }           from '@/core/decorator/tsyringe';
import { Injectable }       from '@/core/decorator/tsyringe';

@Injectable()
export default class StorageService {

  @Inject() mapper!: Mapper;

  resetLocalStorage() {

    localStorage.removeItem(StorageEnum.KEY.ME);
    localStorage.removeItem(StorageEnum.KEY.MENU);
    localStorage.removeItem(StorageEnum.KEY.AUTH_TOKEN);
  }

  setLocalStorage(key: StorageEnum.KEY, params: any) {

    if (!!params) {
      if (typeof params === StorageEnum.TYPE.OBJECT) {
        localStorage.setItem(key, JSON.stringify(params));
      } else {
        localStorage.setItem(key, params);
      }
    }
  }

  getLocalStorage<T>(key: StorageEnum.KEY, type?: StorageEnum.TYPE, classType?: ClassConstructor<T>): any {

    let   returnValue: any           = null;
    const storage    : string | null = localStorage.getItem(key);

    if (!!storage) {
      switch (type) {
        case StorageEnum.TYPE.STRING:
          returnValue = storage;
          break;

        case StorageEnum.TYPE.NUMBER:
          returnValue = parseInt(storage, 10);
          break;

        case StorageEnum.TYPE.BOOLEAN:
          switch (storage) {
            case 'false':
              returnValue = false;
              break;

            case 'true':
              returnValue = true;
              break;

            default:
          }
          break;

        case StorageEnum.TYPE.OBJECT:
          returnValue = !!classType ? this.mapper.toObject<T>(classType, JSON.parse(storage)) : JSON.parse(storage);
          break;

        case StorageEnum.TYPE.ARRAY:
          returnValue = !!classType ? this.mapper.toArray(classType, JSON.parse(storage)) : JSON.parse(storage);
          break;

        default:
          returnValue = storage;
          break;
      }
    }

    return returnValue;
  }
}

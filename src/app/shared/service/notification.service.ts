import                           'reflect-metadata';
import   React              from 'react';
import { ReactNode        } from 'react';
import { AxiosError       } from 'axios';
import { notification     } from 'antd';

import   UtilService        from '@/app/shared/service/util.service';
import   CookieService      from '@/app/shared/service/cookie.service';
import   StorageService     from '@/app/shared/service/storage.service';
import   ERROR_MESSAGE      from '@/app/shared/config/notification.config';
import { CookieEnum       } from '@/app/shared/enum/cookie.enum';
import { Notification     } from '@/app/shared/model/notification.model';
import { Inject           } from '@/core/decorator/tsyringe';
import { Injectable       } from '@/core/decorator/tsyringe';

@Injectable()
export default class NotificationService {

  @Inject() utilService!   : UtilService;
  @Inject() cookieService! : CookieService;
  @Inject() storageService!: StorageService;

  onNotification(config: Notification.Message) {

    notification.open({
      type       : config.type,
      placement  : config.placement,
      message    : [NotificationService.getMessage(config.message)],
      description: this.getDescription(config.description),
      // duration: 100
    });
  }

  onError(error: AxiosError) {
    if (!!error && !!error.response) {
      if (!!error.response.status) {
        switch (true) {
          case error.response.status === 400:
            if (!!error.response.data) {
              this.onNotification(ERROR_MESSAGE.ERROR_CODE(error.response.status, error.response.data.code));
            } else {
              this.onNotification(ERROR_MESSAGE.ERROR(error.response.status));
            }
            break;

          case error.response.status === 401 || error.response.status === 403:
            this.onNotification(ERROR_MESSAGE.UNAUTHORIZED());
            this.cookieService.removeCookie(CookieEnum.KEY.AUTH_TOKEN);
            this.cookieService.removeCookie(CookieEnum.KEY.MENU);
            this.storageService.resetLocalStorage();
            window.history.pushState({}, '', '/auth/login');
            break;

          case error.response.status === 500:
            this.onNotification(ERROR_MESSAGE.ERROR_500(error.response.status));
            break;

          case error.response.status > 400 && error.response.status < 600:
            this.showErrorMessage(error);
            break;

          default:
            this.onNotification(ERROR_MESSAGE.ERROR_CODE(error.response.status, error.response.data.code));
            break;
        }
      } else {
        this.onNotification(ERROR_MESSAGE.ERROR('Timeout'));
      }
    } else {
      this.onNotification(ERROR_MESSAGE.ERROR('Error'));
    }
  }

  private showErrorMessage(error: AxiosError) {

    if (!!error && !!error.response) {
      if (!this.utilService.safeCall(error, error.response.data.code)) {
        this.onNotification(ERROR_MESSAGE.ERROR_CODE(error.response.status, error.response.data.code));
      } else {
        this.onNotification(ERROR_MESSAGE.ERROR(error.response.status));
      }
    }
  }

  private static getMessage(title: string): ReactNode {

    const h = React.createElement;

    return h(
      'div',
      {key: 0, className: ['d-flex', 'flex-grow-1', 'align-items-baseline', 'mr-2']},
      [
        h('strong', {key: 1, className: 'mr-2'}, title),
        h('small', {key: 2, className: 'ml-auto text-italics'}, ''),
      ],
    );
  }

  private getDescription(contents: string[]): ReactNode {

    const h                        = React.createElement;
    const nodes: React.ReactNode[] = [];

    contents.forEach((content: string, index: number) => {
      nodes.push(h('li', {key: index}, content));
    });

    return h('ul', {className: ['left', 'mb-0', 'ml-1']}, nodes);
  }
}

import                        'reflect-metadata';

import { GuideEnum     } from '@/app/guide/enum/guide.enum';
import { Attribute     } from '@/core/decorator/attribute.decorator';
import { IsArray       } from '@/core/decorator/validator';
import { IsNumber      } from '@/core/decorator/validator';
import { IsString      } from '@/core/decorator/validator';
import { IsBoolean     } from '@/core/decorator/validator';
import { IsNotEmpty    } from '@/core/decorator/validator';
import { ArrayNotEmpty } from '@/core/decorator/validator';

export namespace Guide {
  export class Form {
    @Attribute('Input Text')
    @IsString() @IsNotEmpty()
    inputText!: string;

    @Attribute('Input Number')
    @IsNumber() @IsNotEmpty()
    inputNumber!: number;

    @Attribute('Enum Select')
    @IsString() @IsNotEmpty()
    enumSelect!: GuideEnum.Guide;

    @Attribute('Input Text Disable')
    @IsString() @IsNotEmpty()
    inputTextDisable: string = 'input Text Disable';

    @Attribute('Switch')
    @IsBoolean() @IsNotEmpty()
    switch: boolean = true;

    @Attribute('Date Picker')
    @IsString() @IsNotEmpty()
    datePicker!: string;

    @Attribute('Range Picker')
    @IsArray() @ArrayNotEmpty()
    rangePicker!: string[];

    @Attribute('Text Area')
    @IsString() @IsNotEmpty()
    textArea!: string;
  }
}

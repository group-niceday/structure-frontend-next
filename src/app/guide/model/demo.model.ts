import                     'reflect-metadata';
import { DemoEnum   } from '@/app/guide/enum/demo.enum';
import { Pageable   } from '@/app/shared/model/pageable.model';
import { Attribute  } from '@/core/decorator/attribute.decorator';
import { IsString   } from '@/core/decorator/validator';
import { IsNotEmpty } from '@/core/decorator/validator';

export namespace Demo {
  export namespace Request {
    export class Search extends Pageable.Request.Search {
      @Attribute('이름')
      name!: string;

      constructor(options?: Pageable.Request.Options) {
        super(options);
      }
    }

    export class Add {
      @Attribute('이름')
      @IsString() @IsNotEmpty()
      name!: string;

      @Attribute('소속사명')
      @IsString() @IsNotEmpty()
      agencyName!: string;

      @Attribute('생일')
      @IsString() @IsNotEmpty()
      birthDate!: string;

      @Attribute('설명')
      @IsString() @IsNotEmpty()
      description!: string;

      @Attribute('국적')
      @IsString() @IsNotEmpty()
      nationalityType!: DemoEnum.NATIONALITY_TYPE;
    }

    export class Modify {
      @Attribute('아이디')
      id!: number;

      @Attribute('이름')
      @IsString() @IsNotEmpty()
      name!: string;

      @Attribute('소속사명')
      @IsString() @IsNotEmpty()
      agencyName!: string;

      @Attribute('생일')
      @IsString() @IsNotEmpty()
      birthDate!: string;

      @Attribute('설명')
      @IsString() @IsNotEmpty()
      description!: string;

      @Attribute('국적')
      @IsString() @IsNotEmpty()
      nationalityType!: DemoEnum.NATIONALITY_TYPE;
    }
  }

  export namespace Response {
    export class FindAll {
      @Attribute('아이디')
      id!: number;

      @Attribute('이름')
      name!: string;

      @Attribute('소속사명')
      agencyName!: string;

      @Attribute('생일')
      birthDate!: string;

      @Attribute('설명')
      description!: string;

      @Attribute('국적')
      nationalityType!: DemoEnum.NATIONALITY_TYPE;
    }

    export class FindOne {
      @Attribute('아이디')
      id!: number;

      @Attribute('이름')
      name!: string;

      @Attribute('생일')
      birthDate!: string;

      @Attribute('소속사명')
      agencyName!: string;

      @Attribute('설명')
      description!: string;

      @Attribute('국적')
      nationalityType!: DemoEnum.NATIONALITY_TYPE;
    }
  }
}

import   React            from 'react';
import { Formik         } from 'formik';
import { Descriptions   } from 'antd';
import { Form           } from 'antd';

import   Container        from '@/core/container';
import   Mapper           from '@/core/service/mapper.service';
import   NInput           from '@/app/shared/components/form/n-input';
import   NSwitch          from '@/app/shared/components/form/n-switch';
import   NConfirm         from '@/app/shared/components/n-confirm';
import   NTextArea        from '@/app/shared/components/form/n-textArea';
import   NEnumSelect      from '@/app/shared/components/n-enum-select';
import   NDatePicker      from '@/app/shared/components/form/n-date-picker';
import   NDateRangePicker from '@/app/shared/components/form/n-date-range-picker';
import { FormEnum       } from '@/app/shared/enum/form.enum';
import { GuideEnum      } from '@/app/guide/enum/guide.enum';
import { ActionEnum     } from '@/app/shared/enum/action.enum';
import { Guide          } from '@/app/guide/model/guide.model';

const GuideForm = () => {

  const guideForm: Guide.Form = new Guide.Form();
  const mapper   : Mapper     = Container.resolve<Mapper>(Mapper);

  const submit = (values: Guide.Form) => {

    console.log(mapper.toObject(Guide.Form, values));
  };

  return (
    <Formik initialValues={guideForm} onSubmit={submit}>
      {({ values, handleChange, setFieldValue }) =>
        (((values = mapper.toObject(Guide.Form, values)),
            <Form labelCol={{ span: 24 }} wrapperCol={{ span: 10 }}>
              <Form.Item label="Input Text" name="inputText" required>
                <NInput onChange={handleChange}
                        value={values.inputText}
                        type={FormEnum.INPUT_TYPE.TEXT}
                        placeholder="Input Text"
                        name="inputText"
                        parent={values} />
              </Form.Item>

              <Form.Item label="Input Number" name="inputNumber" required>
                <NInput onChange={handleChange}
                        value={values.inputNumber}
                        type={FormEnum.INPUT_TYPE.NUMBER}
                        placeholder="Input Number"
                        name="inputNumber"
                        parent={values} />
              </Form.Item>

              <Form.Item label="Enum Select" required>
                <NEnumSelect label="enumSelect"
                             placeholder="Enum Select"
                             value={values.enumSelect}
                             setFieldValue={setFieldValue}
                             enumType={GuideEnum.Guide} />
              </Form.Item>

              <Form.Item label="Input Text Disable">
                <NInput value={values.inputTextDisable} type={FormEnum.INPUT_TYPE.TEXT} disabled />
              </Form.Item>

              <Form.Item label="Date Picker">
                <NDatePicker name="datePicker"
                             value={values.datePicker}
                             setFieldValue={setFieldValue}/>
              </Form.Item>

              <Form.Item label="Range Picker">
                <NDateRangePicker name="rangePicker"
                                  value={values.rangePicker}
                                  setFieldValue={setFieldValue}/>
              </Form.Item>

              <Form.Item label="Switch" valuePropName="checked">
                <NSwitch field="switch"
                         checkedLabel="On"
                         unCheckedLabel="Off"
                         initial={values.switch}
                         setFieldValue={setFieldValue} />
              </Form.Item>

              <Form.Item label="Text Area">
                <NTextArea rows={4}
                           name="textArea"
                           placeholder="TextArea"
                           parent={values}
                           value={values.textArea}
                           onChange={handleChange}
                           maxLength={100} />
              </Form.Item>

              <Form.Item label="Confirm Button">
                <NConfirm submit={submit} values={values} type={ActionEnum.RESULT.ENUM.ADD} />
                <NConfirm submit={submit} values={values} type={ActionEnum.RESULT.ENUM.UPDATE} />
                <NConfirm submit={submit} values={values} type={ActionEnum.RESULT.ENUM.DELETE} />
              </Form.Item>

              <Descriptions title="detail">
                <Descriptions.Item label="Input Text">
                  {values.inputText}
                </Descriptions.Item>

                <Descriptions.Item label="Input Number">
                  {values.inputNumber}
                </Descriptions.Item>

                <Descriptions.Item label="Enum Select">
                  {values.enumSelect}
                </Descriptions.Item>

                <Descriptions.Item label="Input Text Disable">
                  {values.inputTextDisable}
                </Descriptions.Item>

                <Descriptions.Item label="Switch">
                  {JSON.stringify(values.switch)}
                </Descriptions.Item>

                <Descriptions.Item label="Date Picker">
                  {values.datePicker}
                </Descriptions.Item>

                <Descriptions.Item label="Range Picker">
                  {values.rangePicker && values.rangePicker[0]}
                  {values.rangePicker && '~'}
                  {values.rangePicker && values.rangePicker[1]}
                </Descriptions.Item>

                <Descriptions.Item label="Text Area">
                  {values.textArea}
                </Descriptions.Item>
              </Descriptions>
            </Form>
        ))}
    </Formik>
  );
};

export default GuideForm;

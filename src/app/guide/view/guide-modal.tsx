import   React            from 'react';
import { useDispatch    } from 'react-redux';
import { Button         } from 'antd';
import { Space          } from 'antd';

import   buttonStyle      from '@/assets/css/Button.module.css';
import   NModal           from '@/app/shared/components/n-modal';
import { setModal       } from '@/app/shared/store/modal.slice';
import { useAppSelector } from '@/hooks';

const GuideModal = () => {

  const dispatch         = useDispatch();
  const isModal: boolean = useAppSelector(state => state.modalSlice.isModal);

  return (
    <>
      <Button className={buttonStyle.info} onClick={() => dispatch(setModal())}>Modal</Button>

      {isModal && (
        <NModal title="guideModal" width={1000}>
          <p>Content</p>

          <Space size="small">
            <Button className={buttonStyle.primary} size="large" onClick={() => console.log('모달 확인버튼')}>
              확인
            </Button>
            <Button className={buttonStyle.danger} size="large" onClick={() => dispatch(setModal())}>
              취소
            </Button>
          </Space>
        </NModal>
      )}
    </>
    );
};

export default GuideModal;

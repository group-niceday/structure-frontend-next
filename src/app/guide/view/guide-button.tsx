import   React              from 'react';
import { useState }         from 'react';
import { SizeType }         from 'antd/es/config-provider/SizeContext';
import { SpaceSize }        from 'antd/es/space';
import { DownloadOutlined } from '@ant-design/icons';
import { Button }           from 'antd';
import { Radio }            from 'antd';
import { Space }            from 'antd';
import { Card }             from 'antd';

import   buttonStyle        from '@/assets/css/Button.module.css';

const GuideButton = () => {

  const [buttonSize, setButtonSize] = useState<SizeType>('large');
  const [spaceSize, setSpaceSize]   = useState<SpaceSize | [SpaceSize, SpaceSize]>('small');

  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Space>
        Button Size :
        <Radio.Group value={buttonSize} onChange={e => setButtonSize(e.target.value)}>
          <Radio.Button value="large">Large</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="small">Small</Radio.Button>
        </Radio.Group>
      </Space>

      <Space>
        Space Size :
        <Radio.Group value={spaceSize} onChange={e => setSpaceSize(e.target.value)}>
          <Radio value="small">Small</Radio>
          <Radio value="middle">Middle</Radio>
          <Radio value="large">Large</Radio>
        </Radio.Group>
      </Space>

      <Card title="button" bordered={false}>
        <Space size={spaceSize}>
          <Button className={buttonStyle.primary} size={buttonSize}>primary</Button>
          <Button className={buttonStyle.info} size={buttonSize}>info</Button>
          <Button className={buttonStyle.warning} size={buttonSize}>warning</Button>
          <Button className={buttonStyle.success} size={buttonSize}>success</Button>
          <Button className={buttonStyle.danger} size={buttonSize}>danger</Button>
          <Button className={buttonStyle.secondary} size={buttonSize}>secondary</Button>
          <Button size={buttonSize} disabled>disabled</Button>
        </Space>
      </Card>

      <Card title="lint button" bordered={false}>
        <Space size={spaceSize}>
          <Button className={buttonStyle.primaryLine} size={buttonSize}>primary</Button>
          <Button className={buttonStyle.infoLine} size={buttonSize}>info</Button>
          <Button className={buttonStyle.warningLine} size={buttonSize}>warning</Button>
          <Button className={buttonStyle.successLine} size={buttonSize}>success</Button>
          <Button className={buttonStyle.dangerLine} size={buttonSize}>danger</Button>
          <Button className={buttonStyle.secondaryLine} size={buttonSize}>secondary</Button>
        </Space>
      </Card>

      <Card title="download button" bordered={false}>
        <Space size={spaceSize}>
          <Button type="primary" icon={<DownloadOutlined />} size={buttonSize} />
          <Button type="primary" shape="circle" icon={<DownloadOutlined />} size={buttonSize} />
          <Button type="primary" shape="round" icon={<DownloadOutlined />} size={buttonSize} />
          <Button type="primary" shape="round" icon={<DownloadOutlined />} size={buttonSize}>
            Download
          </Button>
          <Button type="primary" icon={<DownloadOutlined />} size={buttonSize}>
            Download
          </Button>
        </Space>
      </Card>
    </Space>
  );
};

export default GuideButton;

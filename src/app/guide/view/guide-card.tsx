import   React        from 'react';
import { Button }     from 'antd';
import { Card }       from 'antd';
import { Row }        from 'antd';
import { Col }        from 'antd';
import { Space }      from 'antd';

import   buttonStyle  from '@/assets/css/Button.module.css';

const GuideCard = () => (

  <>
    <Card title="card" style={{ width: 300, marginBottom: 20 }}>
      <p>Card content</p>
      <p>Card content</p>
      <p>Card content</p>
    </Card>

    <Space>
      <Card title="Default size card" size="default" style={{ marginBottom: 20 }}>
        <p>Card content</p>
        <p>Card content</p>
        <p>Card content</p>
      </Card>
    </Space>

    <Card type="inner" title="Card title" extra={<Button className={buttonStyle.secondary}>버튼</Button>} style={{ marginBottom: 20 }}>
      Inner Card content
    </Card>

    <Row gutter={24}>
      <Col span={8}>
        <Card title="Card title" type="inner">
          Card content
        </Card>
      </Col>
      <Col span={8}>
        <Card title="Card title" type="inner">
          Card content
        </Card>
      </Col>
      <Col span={8}>
        <Card title="Card title" type="inner">
          Card content
        </Card>
      </Col>
    </Row>
  </>
  );

export default GuideCard;

import   React            from 'react';
import { useState       } from 'react';
import { GoogleMap      } from '@react-google-maps/api';
import { LoadScriptNext } from '@react-google-maps/api';
import { MarkerF        } from '@react-google-maps/api';

const GuideMap = () => {

  const [center ] = useState<google.maps.LatLngLiteral>({
    lat: 37.5361,
    lng: 126.897,
  });
  const [marker, setMarker] = useState<google.maps.LatLngLiteral>();
  const containerStyle = {
    width: '800px',
    height: '800px',
  };

  return (
    <>
      <LoadScriptNext googleMapsApiKey={`${process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY}`}>
        <GoogleMap
        mapContainerStyle={containerStyle}
        center={center}
        zoom={18}
        onClick={(e: google.maps.MapMouseEvent) => {
          setMarker({ ...e.latLng!.toJSON() });
        }}
      >
          {marker && <MarkerF position={marker} key={1} />}
        </GoogleMap>
      </LoadScriptNext>
      <p>
        위도 :
        {marker ? marker?.lat : 0}
      </p>
      <p>
        경도 :
        {marker ? marker?.lng : 0}
      </p>
    </>
  );
};

export default GuideMap;

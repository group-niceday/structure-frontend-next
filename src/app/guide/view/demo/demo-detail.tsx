import   React            from 'react';
import { useEffect      } from 'react';
import { useQuery       } from 'react-query';
import { UseQueryResult } from 'react-query';
import { useTranslation } from 'next-i18next';
import { useDispatch    } from 'react-redux';
import { Descriptions   } from 'antd';

import   UtilService      from '@/app/shared/service/util.service';
import { DemoApi        } from '@/pages/api/demo.api';
import { DemoEnum       } from '@/app/guide/enum/demo.enum';
import { Demo           } from '@/app/guide/model/demo.model';
import { offSpinner     } from '@/app/shared/store/spinner.slice';
import { onSpinner      } from '@/app/shared/store/spinner.slice';
import { setModal       } from '@/app/shared/store/modal.slice';

type Props = {
	id: number;
};

const DemoDetail = ({ id }: Props) => {

  const util                 = new UtilService();
  const demoApi: DemoApi     = new DemoApi();
	const dispatch             = useDispatch();
	const { t }                = useTranslation('demo');

	const { data, status }: UseQueryResult<Demo.Response.FindOne> = useQuery(['demos'], () => demoApi.getOne(id));

	useEffect(() => {

		if (status === 'loading') {
			dispatch(onSpinner());
		}
		if (status === 'success' || status === 'error') {
			dispatch(offSpinner());
		}
	}, [status]);

	return (
    status === 'success' ? (
      <Descriptions title={t('mainTitle')}>
        <Descriptions.Item label={t('detail.label.id')}>
          <span onClick={() => dispatch(setModal())}>{data.id}</span>
        </Descriptions.Item>
        <Descriptions.Item label={t('detail.label.name')}>{data.name}</Descriptions.Item>
        <Descriptions.Item label={t('detail.label.agencyName')}>{data.agencyName}</Descriptions.Item>
        <Descriptions.Item label={t('detail.label.birthDate')}>{data.birthDate}</Descriptions.Item>
        <Descriptions.Item label={t('detail.label.description')}>{data.description}</Descriptions.Item>
        <Descriptions.Item label={t('detail.label.nationalityType')}>
          {util.enumDetailHelper(data.nationalityType, DemoEnum.NATIONALITY_TYPE)}
        </Descriptions.Item>
      </Descriptions>
			) : (
  <h1>DemoDetail</h1>
			)
	);
};

export default DemoDetail;

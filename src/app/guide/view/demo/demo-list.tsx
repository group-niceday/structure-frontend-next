import   React            from 'react';
import { useEffect }      from 'react';
import { useRouter }      from 'next/router';
import { useTranslation } from 'next-i18next';
import { Input }          from 'antd';
import { Table }          from 'antd';

import   tableStyle       from '@/assets/css/Table.module.css';
import   UtilService      from '@/app/shared/service/util.service';
import   NPagination      from '@/app/shared/components/n-pagination';
import { DemoEnum }       from '@/app/guide/enum/demo.enum';
import { Demo }           from '@/app/guide/model/demo.model';
import { UseQueryResult } from 'react-query';
import { useQuery }       from 'react-query';
import { DemoApi }        from '@/pages/api/demo.api';
import { Pageable }       from '@/app/shared/model/pageable.model';

type Props = {
	searchProps: Demo.Request.Search;
};

const DemoList = ({ searchProps }: Props) => {

	const router                      = useRouter();
	const { t }                       = useTranslation('demo');
	const util                        = new UtilService();
	const demoApi: DemoApi            = new DemoApi();
	const search: Demo.Request.Search = new Demo.Request.Search();

	useEffect(() => {

		search.page = searchProps.page;
		search.size = searchProps.size;
		search.name = searchProps.name;
	}, [searchProps]);

	const onSearch = (value: string) => {

		router.push({
			pathname: 'demo',
			query: {
				page: 0,
				size: searchProps.size,
				name: value,
			},
		});
	};

	const { data }: UseQueryResult<Pageable.Response.Page<Demo.Response.FindAll>> = useQuery(
		['demos', searchProps],
		() => demoApi.getPage(search),
	);

	const onRow = (record: Demo.Response.FindAll) => ({

			onClick: () => {
				router.push(`/guide/demo/detail/${record.id}`);
			},
		});

	return (
  <>
    <Input.Search
				placeholder={t('list.placeholder.search', { ns: 'demo' }) as string}
				onSearch={onSearch}
				enterButton
				className={tableStyle.searchInput}
			/>
    <Table dataSource={data?.content} rowKey="id" onRow={onRow} pagination={false} className={tableStyle.table}>
      <Table.Column title={t('list.title.name') as string} dataIndex="name" key="name" />
      <Table.Column title={t('list.title.agencyName') as string} dataIndex="agencyName" key="agencyName" />
      <Table.Column title={t('list.title.birthDate') as string} dataIndex="birthDate" key="birthDate" />
      <Table.Column title={t('list.title.description') as string} dataIndex="description" key="description" />
      <Table.Column
					title={t('list.title.nationalityType') as string}
					dataIndex="nationalityType"
					key="nationalityType"
					render={(nationalityType: string) => util.enumDetailHelper(nationalityType, DemoEnum.NATIONALITY_TYPE)}
				/>
    </Table>

    <NPagination total={data?.totalElements}
                 path="demo"
                 searchKey="name"
                 searchValue={router.query.name} />
  </>
	);
};

export default DemoList;

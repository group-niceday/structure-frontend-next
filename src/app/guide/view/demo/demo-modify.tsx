import   React            from 'react';
import { UseQueryResult } from 'react-query';
import { Formik         } from 'formik';
import { useQuery       } from 'react-query';
import { useRouter      } from 'next/router';
import { useTranslation } from 'next-i18next';
import { Form           } from 'antd';
import { Button         } from 'antd';

import   Container        from '@/core/container';
import   Mapper           from '@/core/service/mapper.service';
import   NInput           from '@/app/shared/components/form/n-input';
import   NConfirm         from '@/app/shared/components/n-confirm';
import   NTextArea        from '@/app/shared/components/form/n-textArea';
import   NEnumSelect      from '@/app/shared/components/n-enum-select';
import   NDatePicker      from '@/app/shared/components/form/n-date-picker';
import { FormEnum       } from '@/app/shared/enum/form.enum';
import { ActionEnum     } from '@/app/shared/enum/action.enum';
import { DemoApi        } from '@/pages/api/demo.api';
import { DemoEnum       } from '@/app/guide/enum/demo.enum';
import { Demo           } from '@/app/guide/model/demo.model';

type Props = {
	id             : number;
	fnModifyAccount: (values: Demo.Request.Modify) => void;
};

const DemoModify = ({ id, fnModifyAccount }: Props) => {

	const demoApi: DemoApi = new DemoApi();
  const mapper : Mapper  = Container.resolve<Mapper>(Mapper);
	const router           = useRouter();
	const { t }            = useTranslation(['common', 'demo']);

	const { data, status }: UseQueryResult<Demo.Response.FindOne> = useQuery(['demo'], () => demoApi.getOne(id));

	const submit = async (values: Demo.Request.Modify) => {

		fnModifyAccount(mapper.toObject(Demo.Request.Modify, values));
	};

	const fnGoBack = () => {

		router.back();
	};

	return (
    status === 'success' ? (
      <Formik initialValues={data} onSubmit={submit} >
        {({ values, handleSubmit, handleChange, setFieldValue }) =>
          (((values = mapper.toObject(Demo.Request.Modify, values)),
          <Form labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                onFinish={handleSubmit}
                autoComplete="off" >
            <Form.Item label={t('modify.label.name', { ns: 'demo' })} >
              <NInput value={values.name}
                      name="name"
                      type={FormEnum.INPUT_TYPE.TEXT}
                      placeholder="이름을 입력해 주세요."
                      parent={values}
                      onChange={handleChange} />
            </Form.Item>
            <Form.Item label={t('modify.label.agencyName', { ns: 'demo' })} >
              <NInput value={values.agencyName}
                      name="agencyName"
                      type={FormEnum.INPUT_TYPE.TEXT}
                      placeholder="소속사명을 입력해 주세요."
                      parent={values}
                      onChange={handleChange} />
            </Form.Item>
            <Form.Item label={t('modify.label.birthDate', { ns: 'demo' })} >
              <NDatePicker name="birthDate"
                           value={values.birthDate}
                           setFieldValue={setFieldValue} />
            </Form.Item>
            <Form.Item label={t('modify.label.nationalityType', { ns: 'demo' })} >
              <NEnumSelect placeholder={t('modify.placeholder.nationalityType', { ns: 'demo' })}
                           value={values.nationalityType}
                           setFieldValue={setFieldValue}
                           enumType={DemoEnum.NATIONALITY_TYPE} />
            </Form.Item>
            <Form.Item label={t('modify.label.description', { ns: 'demo' })} >
              <NTextArea rows={4}
                         value={values.description}
                         name="description"
                         placeholder="설명"
                         onChange={handleChange}
                         parent={values}
                         maxLength={100} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }} >
              <NConfirm submit={submit} values={values} type={ActionEnum.RESULT.ENUM.UPDATE} />
              <Button type="primary" htmlType="button" onClick={fnGoBack} >
                {t('button.back', { ns: 'common' })}
              </Button>
            </Form.Item>
          </Form>
        ))}
      </Formik>
      ) : (
      <h1>DemoModify</h1>
    )
	);
};

export default DemoModify;

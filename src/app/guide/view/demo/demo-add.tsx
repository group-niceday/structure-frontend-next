import React              from 'react';
import { useTranslation } from 'next-i18next';
import { useRouter }      from 'next/router';
import { Formik }         from 'formik';
import { Button }         from 'antd';
import { Form }           from 'antd';

import   formStyle        from '@/assets/css/Form.module.css';
import   Container        from '@/core/container';
import   Mapper           from '@/core/service/mapper.service';
import   NInput           from '@/app/shared/components/form/n-input';
import   NTextArea        from '@/app/shared/components/form/n-textArea';
import   NConfirm         from '@/app/shared/components/n-confirm';
import   NEnumSelect      from '@/app/shared/components/n-enum-select';
import   NDatePicker      from '@/app/shared/components/form/n-date-picker';
import { ActionEnum }     from '@/app/shared/enum/action.enum';
import { DemoEnum }       from '@/app/guide/enum/demo.enum';
import { FormEnum }       from '@/app/shared/enum/form.enum';
import { Demo }           from '@/app/guide/model/demo.model';

type Props = {
  fnAddAccount: (values: Demo.Request.Add) => void;
};

const DemoAdd = ({ fnAddAccount }: Props) => {

  const add   : Demo.Request.Add = new Demo.Request.Add();
  const mapper: Mapper           = Container.resolve<Mapper>(Mapper);
  const router                   = useRouter();
  const { t }                    = useTranslation(['common', 'demo']);

  const submit = async (values: Demo.Request.Add) => {

    fnAddAccount(mapper.toObject(Demo.Request.Add, values));
  };

  const fnGoBack = () => {

    router.back();
  };

  return (
    <Formik initialValues={add} onSubmit={submit} >
      {({ values, handleSubmit, handleChange, setFieldValue }) =>
        (((values = mapper.toObject(Demo.Request.Add, values)),
        <Form labelCol={{ span: 2 }}
              wrapperCol={{ span: 10 }}
              onFinish={handleSubmit}
              autoComplete="off"
              className={formStyle.form} >
          <Form.Item label={t('add.label.name', { ns: 'demo' })} required >
            <NInput value={values.name}
                    name="name"
                    placeholder="이름을 입력해 주세요."
                    type={FormEnum.INPUT_TYPE.TEXT}
                    parent={values}
                    onChange={handleChange} />
          </Form.Item>

          <Form.Item label={t('add.label.agencyName', { ns: 'demo' })} required >
            <NInput value={values.agencyName}
                    name="agencyName"
                    type={FormEnum.INPUT_TYPE.TEXT}
                    placeholder="소속사명을 입력해 주세요."
                    parent={values}
                    onChange={handleChange} />
          </Form.Item>

            <Form.Item label={t('add.label.birthDate', { ns: 'demo' })} required>
              <NDatePicker name="birthDate"
                           value={values.birthDate}
                           setFieldValue={setFieldValue}/>
            </Form.Item>

          <Form.Item label={t('add.label.nationalityType', { ns: 'demo' })} required >
            <NEnumSelect label="nationalityType"
                         placeholder={t('add.placeholder.nationalityType', { ns: 'demo' })}
                         value={values.nationalityType}
                         setFieldValue={setFieldValue}
                         enumType={DemoEnum.NATIONALITY_TYPE} />
          </Form.Item>

          <Form.Item label={t('add.label.description', { ns: 'demo' })} required >
            <NTextArea rows={4}
                       value={values.description}
                       name="description"
                       placeholder="설명"
                       onChange={handleChange}
                       parent={values}
                       maxLength={100} />
          </Form.Item>

            <Form.Item>
              <NConfirm submit={submit} values={values} type={ActionEnum.RESULT.ENUM.ADD} />
              <Button type="primary" htmlType="button" onClick={fnGoBack}>
                {t('button.back', { ns: 'common' })}
              </Button>
            </Form.Item>
        </Form>
      ))}
    </Formik>
  );
};

export default DemoAdd;

import { AxiosError         } from 'axios';
import { AxiosRequestConfig } from 'axios';

import   ERROR_MESSAGE        from '@/app/shared/config/notification.config';
import { AxiosAbstract      } from '@/core/axios/axios.abstract';

export default class AuthAxios extends AxiosAbstract {

  constructor() {
    super(`${process.env.NEXT_PUBLIC_AUTH_URL}`);

    this.axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        config.headers!.Authorization = `Basic ${process.env.NEXT_PUBLIC_AUTH_HEADER}`;
        config.headers!['Content-Type'] = process.env.NEXT_PUBLIC_AUTH_CONTENT_TYPE!;

        return config;
      },
      (error: AxiosError) => Promise.reject(error),
    );

    this.axios.interceptors.response.use(
      (config: AxiosRequestConfig) => config,
      (error: AxiosError) => {
        this.notificationService.onNotification(ERROR_MESSAGE.ERROR_LOGIN());

        return Promise.reject(error);
      },
    );
  }
}

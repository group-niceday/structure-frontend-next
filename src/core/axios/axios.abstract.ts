import   qs                   from 'qs';
import   Axios                from 'axios';
import { AxiosInstance      } from 'axios';
import { AxiosResponse      } from 'axios';
import { AxiosRequestConfig } from 'axios';

import   Container            from '@/core/container';
import   NotificationService  from '@/app/shared/service/notification.service';

export abstract class AxiosAbstract {

  notificationService: NotificationService = Container.resolve<NotificationService>(NotificationService);

  axios: AxiosInstance;

  protected constructor(baseURL: string, timeout: number = 1000 * 60) {
    this.axios = Axios.create({
      baseURL,
      timeout,
    });

    this.axios.interceptors.request.use(
      (config: AxiosRequestConfig) => config,
      error => {
        this.notificationService.onError(error);
        return Promise.reject(error);
      },
    );
  }

  get(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.get(url, {
      paramsSerializer: (data: any) => qs.stringify(data, { encode: false, allowDots: true, arrayFormat: 'repeat' }),
      ...options,
    });
  }

  delete(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.delete(url, {
      paramsSerializer: (data: any) => qs.stringify(data, { encode: false, allowDots: true, arrayFormat: 'repeat' }),
      ...options,
    });
  }

  post(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.post(url, params, {
      paramsSerializer: (data: any) => qs.stringify(data, { encode: false, allowDots: true }),
      ...options,
    });
  }

  put(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.put(url, params, {
      paramsSerializer: (data: any) => qs.stringify(data, {encode: false, allowDots: true}),
      ...options,
    });
  }

  patch(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.patch(url, params, {
      paramsSerializer: (data: any) => qs.stringify(data, {encode: false, allowDots: true}),
      ...options,
    });
  }
}

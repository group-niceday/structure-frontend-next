import { AxiosError         } from 'axios';
import { AxiosResponse      } from 'axios';
import { AxiosRequestConfig } from 'axios';

import { CookieEnum         } from '@/app/shared/enum/cookie.enum';
import { AxiosAbstract      } from '@/core/axios/axios.abstract';
import   Container            from '@/core/container';
import   CookieService        from '@/app/shared/service/cookie.service';

export default class DemoAxios extends AxiosAbstract {

  cookie: CookieService = Container.resolve<CookieService>(CookieService);

  constructor() {
    super(`${process.env.NEXT_PUBLIC_DEMO_API_URL}`);

    this.axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        const authToken = this.cookie.getCookie(CookieEnum.KEY.AUTH_TOKEN, CookieEnum.TYPE.OBJECT);

        if (!!authToken) {
          config.headers!.Authorization = `Bearer ${authToken.access_token}`;
        }

        return config;
      },
      (error: AxiosError) => Promise.reject(error),
    );

    this.axios.interceptors.response.use(
      (response: AxiosResponse) => response,
      (error: AxiosError) => {
        const authToken = this.cookie.getCookie(CookieEnum.KEY.AUTH_TOKEN, CookieEnum.TYPE.OBJECT);

        if (!!authToken && !!authToken.refresh_token && !!error.response && error.response.status === 401) {
          // this.broadcast.onDefaultSpinner(false);
          this.cookie.removeCookie(CookieEnum.KEY.AUTH_TOKEN);

          const params = this.getRefresh(authToken);

          return params.then((response: AxiosResponse) => {
            this.cookie.setCookie(CookieEnum.KEY.AUTH_TOKEN, response.data);
            error.config.headers!.Authorization = `Bearer ${response.data.access_token}`;

            return this.axios.request(error.config);
          });
        }
          this.notificationService.onError(error);

          return Promise.reject(error);

      },
    );
  }

  private async getRefresh(authToken): Promise<any> {

    const formData = new FormData();

    formData.append('grant_type', 'refresh_token');
    formData.append('refresh_token', authToken.refresh_token);

    return await this.post('oauth/token', formData, {
      baseURL: `${process.env.NEXT_PUBLIC_AUTH_URL}`,
      headers: {
        Authorization: `Basic ${process.env.NEXT_PUBLIC_AUTH_HEADER}`,
        'Content-Type' : process.env.NEXT_PUBLIC_AUTH_CONTENT_TYPE!,
      },
    });
  }
}

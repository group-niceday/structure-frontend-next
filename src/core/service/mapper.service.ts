import { ClassConstructor } from 'class-transformer';
import { instanceToPlain  } from 'class-transformer';
import { plainToInstance  } from 'class-transformer';

// import { Pageable         } from '@/app/shared/model/pageable.model';
import { Injectable       } from '@/core/decorator/tsyringe';

@Injectable()
export default class Mapper {

  toObject<T>(type: ClassConstructor<T>, source: T): T {

    return plainToInstance(type, source, {excludeExtraneousValues: true, exposeDefaultValues: true});
  }

  toArray<T>(type: ClassConstructor<T>, source: T[]): T[] {

    return plainToInstance(type, source, {excludeExtraneousValues: true, exposeDefaultValues: true});
  }

  // toPage<T>(type: ClassConstructor<T>, source: Pageable.Response.Page<T>): Pageable.Response.Page<T> {
  //
  //   const page = plainToInstance(Pageable.Response.Page, source, {excludeExtraneousValues: true}) as Pageable.Response.Page<T>;
  //
  //   page.content = plainToInstance(type, source.content, {excludeExtraneousValues: true}) as T[];
  //
  //   return page;
  // }

  toPlain<T>(source: T): object {

    return instanceToPlain(source);
  }
}

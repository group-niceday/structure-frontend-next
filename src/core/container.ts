import                    'reflect-metadata';
import { container } from 'tsyringe';

export default class Container {

  static resolve<T>(params): T {

    return container.resolve(params);
  }
}

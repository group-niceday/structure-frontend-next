import { Validator        }  from 'class-validator';
import { ValidationError  }  from 'class-validator';
import { ValidatorOptions }  from 'class-validator';

import   Container           from '@/core/container';
import   ValidateService     from '@/core/service/validate.service';
import   NotificationService from '@/app/shared/service/notification.service';
import { NotificationEnum }  from '@/app/shared/enum/notification.enum';
import { Notification     }  from '@/app/shared/model/notification.model';

const validateService    : ValidateService     = new ValidateService();
const notificationService: NotificationService = Container.resolve<NotificationService>(NotificationService);

export function Validate(validatorOptions?: ValidatorOptions) {

  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    const validator = new Validator();
    const original  = descriptor.value;

    descriptor.value = function (...args: any[]) {
      const errors: ValidationError[] = [];

      args.forEach((arg: any) => {
        errors.push(...validator.validateSync(arg, validatorOptions));
      });

      if (!!errors && errors.length > 0) {
        notificationService.onNotification(new Notification.Message({
          type       : NotificationEnum.TYPE.warning,
          message    : '오류 메시지',
          description: validateService.setErrorMessages(errors),
        }));

        // Todo: 깔끔한 에러 처리 방법 검토
        return Promise.reject(new Error('CLASS_VALIDATOR_ERROR'));
      }

      return original.apply(this, args);
    };
  };
}

import { ValidationOptions                    } from 'class-validator';
import { IsBoolean as ClassValidatorIsBoolean } from 'class-validator';

import { VALIDATE_CONFIG                      } from '@/core/config/validate.config';

export function IsBoolean(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isBoolean, ...validationOptions ?? {}};

    ClassValidatorIsBoolean(validationOptions)(target, propertyKey);
  };
}

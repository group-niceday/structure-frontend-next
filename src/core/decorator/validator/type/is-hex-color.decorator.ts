import { ValidationOptions                      } from 'class-validator';
import { IsHexColor as ClassValidatorIsHexColor } from 'class-validator';

import { VALIDATE_CONFIG                        } from '@/core/config/validate.config';

export function IsHexColor(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isHexColor, ...validationOptions ?? {}};

    ClassValidatorIsHexColor(validationOptions)(target, propertyKey);
  };
}

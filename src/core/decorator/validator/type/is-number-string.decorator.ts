import   ValidatorJS                                      from 'validator';
import { ValidationOptions                              } from 'class-validator';
import { IsNumberString as ClassValidatorIsNumberString } from 'class-validator';

import { VALIDATE_CONFIG                                } from '@/core/config/validate.config';

export function IsNumberString(options?: ValidatorJS.IsNumericOptions, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isNumberString, ...validationOptions ?? {}};

    ClassValidatorIsNumberString(options, validationOptions)(target, propertyKey);
  };
}

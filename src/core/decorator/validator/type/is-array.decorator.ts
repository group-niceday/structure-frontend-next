import { ValidationOptions                } from 'class-validator';
import { IsArray as ClassValidatorIsArray } from 'class-validator';

import { VALIDATE_CONFIG                  } from '@/core/config/validate.config';

export function IsArray(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isArray, ...validationOptions ?? {}};

    ClassValidatorIsArray(validationOptions)(target, propertyKey);
  };
}

import { ValidationOptions            } from 'class-validator';
import { ValidatorConstraintInterface } from 'class-validator';
import { IsNotEmpty                   } from 'class-validator';
import { registerDecorator            } from 'class-validator';
import { ValidatorConstraint          } from 'class-validator';
import { ExposeOptions                } from 'class-transformer/types/interfaces';
import { Exclude                      } from 'class-transformer';

import { VALIDATE_CONFIG              } from '@/core/config/validate.config';

export function IsValid(options?: { expose?: ExposeOptions, validation?: ValidationOptions }) {

  return (target: any, propertyKey: string) => {
    Exclude({toPlainOnly: true})(target, propertyKey);
    IsNotEmpty()(target, propertyKey);
    IsValidDecorator(options?.validation)(target, propertyKey);
  };
}

function IsValidDecorator(validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsValidValidate,
    });
  };
}

@ValidatorConstraint({name: 'isValid'})
class IsValidValidate implements ValidatorConstraintInterface {

  validate(validator: () => boolean): boolean {

    return validator();
  }

  defaultMessage() {

    return VALIDATE_CONFIG.MESSAGES.isValid;
  }
}

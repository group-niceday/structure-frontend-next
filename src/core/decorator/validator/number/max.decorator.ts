import { ValidationOptions        } from 'class-validator';
import { Max as ClassValidatorMax } from 'class-validator';

import { VALIDATE_CONFIG          } from '@/core/config/validate.config';

export function Max(maxValue: number, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.max, ...validationOptions ?? {}};

    ClassValidatorMax(maxValue, validationOptions)(target, propertyKey);
  };
}
